/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
#ifndef __GPA_FILE_H__
#define __GPA_FILE_H__

BEGIN_GNOME_DECLS

gchar * gpa_file_dialog (const gchar *message, gboolean warn_overwrite);

END_GNOME_DECLS

#endif /* __GPA_FILE_H__ */

