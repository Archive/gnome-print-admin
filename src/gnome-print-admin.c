/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Copyright (C) 2000 Jose M Celorio
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Authors :
 *  Chema Celorio <chema@celorio.com>
 *
 */

#include "gpa-defs.h"

#include <glib.h>
#include <gtk/gtkwidget.h>
#include <gtk/gtkmain.h>
#include <libgnome/gnome-popt.h>
#include <libgnome/gnome-i18n.h>
#include <libgnome/gnome-util.h>
#include <libgnomeui/gnome-init.h>

#include <glade/glade.h>

#include <libgpa/gpa-vendor.h>
#include <libgpa/gpa-printer.h>
#include <libgpa/gpa-tags.h>
#include <libgpa/gpa-ppd.h>

#include "gpa-window.h"


gboolean debug_turned_on = FALSE;
static gchar *ppd_file_name = NULL;

enum {GPA_ARG_NONE, GPA_ARG_FILE, GPA_ARG_LAST};

static gint gpa_main_convert_ppd (const gchar *file_name);

static const struct poptOption options[] =
{
     { "debug", '\0',   POPT_ARG_NONE,  &debug_turned_on, 0,
       N_("Show window debugging messages."),
       NULL },
     
     { "ppd", '\0', POPT_ARG_STRING, &ppd_file_name, 0,
       N_("Convert a ppd file to its XML equivalent"),
       N_("FILE") },

     { NULL, '\0', 0, NULL, 0, NULL, NULL }
};


int
main (int argc, char *argv[])
{
	poptContext popt_context;	/* the popt context */
	GList *printers = NULL;

#if 0	/* Kill warning */
	/* Initialize i18n */
	bindtextdomain (PACKAGE, GNOMELOCALEDIR);
	textdomain (PACKAGE);
#endif	

	gnome_init_with_popt_table ("gnome-print-admin", VERSION, argc,
				    argv, options, 0, &popt_context);

	/* Init Gnome + LibGlade */
	glade_gnome_init ();
  
	if (!gpa_printers_list_load (&printers))
		return 0;

	/* Just convert the ppd file*/
	if (ppd_file_name != NULL)
		return 	gpa_main_convert_ppd (ppd_file_name);

	if (gpa_window_create (&printers))
		gtk_main ();

	poptFreeContext (popt_context);
	
	return 0;
}


static gint
gpa_main_convert_ppd (const gchar *file_name)
{
	GpaModel *model;
		
	model = gpa_ppd_add_model (file_name);
	if (model == NULL) {
		g_warning ("Could not add model, aborting\n");
		return -1;
	}
	
#if 0	

	/* Create the model info and add it to the list */
	model_info = gpa_model_info_new (gpa_model_get_name (model),
					 gpa_model_get_id (model),
					 gpa_model_get_vendor (model));
	
	if (model_info == NULL) {
		g_warning ("Could not create model info");
	}
	
	gpa_add_printer ("Printer from PPD", model_info, adi->wi);
#endif
	g_print ("PPd converted ..\n");
	
	return 0;
}
	
	
