/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Copyright (C) 2000 Jose M Celorio
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Authors :
 *  Chema Celorio <chema@celorio.com>
 *
 */

#include "gpa-defs.h"

#include <glib.h>
#include <gtk/gtkmain.h>
#include <gtk/gtkwidget.h>
#include <gtk/gtkclist.h>
#include <gtk/gtknotebook.h>

#include <libgnome/gnome-util.h>
#include <libgnomeui/gnome-dialog.h>
#include <libgnomeui/gnome-dialog-util.h>
#include <libgnomeui/gnome-uidefs.h>
#include <glade/glade-xml.h>

#include "gpa-config.h"
#include "gpa-config-settings.h"
#include "gpa-items-create.h"
#include "gpa-items-update.h"

#include <libgpa/libgpa.h>

static void
gpa_dialog_destroy (GtkWidget *dialog, gpointer data)
{
	debug (FALSE, "");
	g_return_if_fail (GNOME_IS_DIALOG (dialog));
	gtk_widget_destroy (dialog);
}

void
gpa_config_set_sensitivity (GpaConfigDialogInfo *cdi)
{
	gboolean selected;
	
	g_return_if_fail (cdi != NULL);
	g_return_if_fail (GNOME_IS_DIALOG (cdi->dialog));

	selected = (cdi->selected_settings == NULL) ? FALSE : TRUE;

	g_return_if_fail (GTK_IS_BUTTON (cdi->select_button));
	
	gtk_widget_set_sensitive (cdi->select_button, selected);
	gtk_widget_set_sensitive (cdi->copy_button, selected);
	gtk_widget_set_sensitive (cdi->rename_button, selected);

	/* We need 2 settings to enable the delete button */
	if (g_list_length (GTK_CLIST (cdi->clist)->row_list) < 2)
		selected = FALSE;
	gtk_widget_set_sensitive (cdi->delete_button, selected);

}

static gboolean
gpa_load_general_page (GpaConfigDialogInfo *cdi)
{
	const gchar *printer_name;
	const gchar *model_name;
	const GpaModel *model;
	GpaPrinter *printer;
	GtkWidget *printer_label;
	GtkWidget *model_label;
	GladeXML *gui;

	debug (FALSE, "");

	g_return_val_if_fail (cdi != NULL, FALSE);
	g_return_val_if_fail (GLADE_IS_XML (cdi->gui), FALSE);
	g_return_val_if_fail (GPA_IS_PRINTER (cdi->printer), FALSE);

	gui = cdi->gui;
	printer_label    = glade_xml_get_widget (gui, "printer_label");
	model_label      = glade_xml_get_widget (gui, "model_label");

	/* This type check is like cheking for NULL, only better */
	g_return_val_if_fail (GTK_IS_LABEL (printer_label), FALSE);
	g_return_val_if_fail (GTK_IS_LABEL (model_label), FALSE);

	/* Set the labels */
	printer = cdi->printer;
	printer_name = gpa_printer_get_name (printer);
	model        = gpa_printer_get_model (printer);
	model_name   = gpa_model_get_info (model, GPA_TAG_MODEL_NAME);

	if ( (printer_name == NULL) ||
	     (model_name   == NULL))
		gpa_error ("Could not get the required strings from the printer "
			   "to create the general page");


	gtk_label_set_text (GTK_LABEL (printer_label), printer_name);
	gtk_label_set_text (GTK_LABEL (model_label),   model_name);

	gpa_load_general_page_settings_stuff (cdi);

#if 0	
	gpa_config_backend_option_menu_attach (cdi->printer, table, cdi->widgets);
#endif

	return TRUE;
}

static void
gpa_config_ok_clicked (gpointer dummy, GpaConfigDialogInfo *cdi)
{
	g_return_if_fail (cdi != NULL);
	g_return_if_fail (GPA_IS_PRINTER (cdi->printer));


	if (!gpa_settings_list_verify ( gpa_printer_settings_list_get (cdi->printer), FALSE)) {
		GtkWidget *err_dialog;
		gchar *error;
		error =  g_strdup_printf ("Conflicting options where found in "
					  "the printer configuration.\n"
					  "(TODO : this should not happen "
					  "since the conflicts are going to be "
					  "auto-solved	)");
		err_dialog = gnome_warning_dialog (error);
		gtk_widget_show (err_dialog);
		g_free (error);
		return;
	}
	gpa_printer_settings_list_swap (cdi->real_printer, cdi->printer);
	/* FIXME :
	   gtk_object_unref (printer);
	*/
	gpa_dialog_destroy (cdi->dialog, NULL);
}

static void
gpa_config_cancel_clicked (gpointer dummy, GpaConfigDialogInfo *cdi)
{
	g_return_if_fail (cdi != NULL);
	g_return_if_fail (GPA_IS_PRINTER (cdi->printer));

	gpa_dialog_destroy (cdi->dialog, NULL);
}


static gboolean
gpa_config_dialog_load_pages (GpaConfigDialogInfo *cdi)
{
	GpaSettings *settings;
	gchar *title;

	debug (FALSE, "");

	g_return_val_if_fail (GLADE_IS_XML (cdi->gui), FALSE);
	g_return_val_if_fail (GPA_IS_PRINTER (cdi->printer), FALSE);
	g_return_val_if_fail (GPA_IS_PRINTER (cdi->real_printer), FALSE);
	
	cdi->dialog   = glade_xml_get_widget (cdi->gui, "config_dialog");
	cdi->notebook = glade_xml_get_widget (cdi->gui, "notebook");
	
	/* This is like checking for null, but we also check type */
	if (!GNOME_IS_DIALOG (cdi->dialog)) {
		gpa_error ("config_dialog type error, expected gnome_dialog");
		return FALSE;
	}
	if (!GTK_IS_NOTEBOOK (cdi->notebook)) {
		gpa_error ("notebook type error, expected gtk_notebook");
		return FALSE;
	}

	/*connect signals */
	gtk_signal_connect (GTK_OBJECT (cdi->dialog), "destroy",
			    GTK_SIGNAL_FUNC (gpa_dialog_destroy),  NULL);
	gtk_signal_connect (GTK_OBJECT (cdi->dialog), "delete_event",
			    GTK_SIGNAL_FUNC (gtk_false),  NULL);

	/* Set the window title */
	title = g_strdup_printf ("Printer Configuration : %s", gpa_printer_get_name (cdi->printer));
	gtk_window_set_title (GTK_WINDOW (cdi->dialog), title);
	g_free (title);

	settings = gpa_printer_settings_get_selected (cdi->printer);
	
	cdi->widgets = g_hash_table_new (g_str_hash, g_str_equal);
	cdi->page = 1;
	cdi->loaded_settings = settings;
	cdi->selected_settings = NULL;
	
	if (!gpa_load_general_page (cdi))
		return FALSE;
	if (!gpa_load_paper_page   (cdi))
		return FALSE;
	if (!gpa_load_generic_page (cdi, GPA_GROUP_QUALITY, _("Print Quality")))
		return FALSE;
	if (!gpa_load_generic_page (cdi, GPA_GROUP_COMPRESSION, _("Compression")))
		return FALSE;
	if (!gpa_load_generic_page (cdi, GPA_GROUP_PS, _("Postscript")))
		return FALSE;
	if (!gpa_load_generic_page (cdi, GPA_GROUP_INSTALLED, _("Installed Options")))
		return FALSE;

	/* Load Buttons, the connect signal is not working, I guess it has to
	 * do with the fact that it is a gnome-dialog
	 */
	cdi->ok_button     = glade_xml_get_widget (cdi->gui, "ok_button");
	cdi->cancel_button = glade_xml_get_widget (cdi->gui, "cancel_button");
	/* Connect by hand */
	gnome_dialog_button_connect (GNOME_DIALOG (cdi->dialog), 0,
				     gpa_config_ok_clicked, cdi);
	gnome_dialog_button_connect (GNOME_DIALOG (cdi->dialog), 1,
				     gpa_config_cancel_clicked, cdi);

	/* Emit a changed settings signal to verify that everything
	 * is fine (widgets loaded and with valid pointers). Set to
	 * NULL afterwards since ther isn't a selected settings in the clist
	 */
	gpa_config_settings_changed (cdi, cdi->loaded_settings);
	
        /* Set sensitivity */
	cdi->selected_settings = NULL;
	gpa_config_set_sensitivity (cdi);
		
	/* Show the dialog and all it's children */
	gtk_widget_show_all (cdi->dialog);
	
	return TRUE;
}

static gboolean
gpa_config_dialog_create (GpaPrinter *printer, GpaPrinter *real_printer)
{
	GpaConfigDialogInfo *cdi;
	GladeXML *gui;
	
	debug (FALSE, "");

	g_return_val_if_fail (GPA_IS_PRINTER (printer), FALSE);

	if (!g_file_exists (GPA_GLADE_DIR GPA_GLADE_FILE)) {
		g_warning ("Could not find %s", GPA_GLADE_DIR GPA_GLADE_FILE);
		return FALSE;
	}
	gui = glade_xml_new (GPA_GLADE_DIR GPA_GLADE_FILE,  NULL);
	if (!gui) {
		gpa_error ("Could not find " GPA_GLADE_FILE);
		return FALSE;
	}


	cdi = g_new (GpaConfigDialogInfo, 1);
	cdi->gui = gui;
	cdi->printer = printer;
	cdi->real_printer = real_printer;
	cdi->rename_dialog = NULL;
	cdi->selected_settings = NULL;
	cdi->loaded_settings = NULL;

	cdi->settings_entry = NULL;
	cdi->command_entry = NULL;
	
	cdi->ok_button = NULL;
	cdi->cancel_button = NULL;
	cdi->select_button = NULL;
	cdi->delete_button = NULL;
	cdi->rename_button = NULL;
		
	gpa_config_dialog_load_pages (cdi);

	return TRUE;
}

gboolean
gpa_config_button_clicked (GpaPrinter *printer)
{
	GpaPrinter *copy_of_printer;

	g_return_val_if_fail (GPA_IS_PRINTER (printer), FALSE);
	
	/* This is what we do :
	   1. We make a copy of the settings, which we use to build the dialog
	   2. If the user cancels the properties dialog. We just
	   dispose, the copy.
	   3. If the user clicks [OK], we swap the settings, and dispose the copy
	*/
	copy_of_printer = gpa_printer_copy (printer);

	g_return_val_if_fail (GPA_IS_PRINTER (copy_of_printer), FALSE);
			      
	gpa_config_dialog_create (copy_of_printer, printer);

	return TRUE;
}
