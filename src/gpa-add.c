/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Copyright (C) 2000 Jose M Celorio
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Authors :
 *  Chema Celorio <chema@celorio.com>
 *
 */

#include "gpa-defs.h"

#include <glib.h>
#include <gtk/gtkmain.h>
#include <gtk/gtksignal.h>
#include <gtk/gtkclist.h>
#include <gtk/gtkentry.h>
#include <libgnome/gnome-util.h>
#include <libgnomeui/gnome-dialog.h>
#include <glade/glade-xml.h>

#include <unistd.h> /* For getpid () */
#include <time.h>   /* For time()   */

/*#include "gpa-private.h"*/
#include "gpa-window.h"
#include <libgpa/gpa-printer.h>
#include "gpa-add.h"
#include "gpa-file.h"
#include <libgpa/gpa-vendor.h>
#include <libgpa/gpa-model.h>
#include <libgpa/gpa-model-info.h>
#include <libgpa/gpa-ppd.h>

static void
gpa_add_dialog_hide (GtkWidget *dialog, gpointer data)
{
	debug (FALSE, "");
	g_return_if_fail (GNOME_IS_DIALOG (dialog));
	gnome_dialog_close (GNOME_DIALOG (dialog));
}

static void
gpa_add_dialog_hide_by_event (GtkWidget *dialog, gpointer dummy, gpointer dummy2)
{
	debug (FALSE, "");
	gpa_add_dialog_hide (dialog, NULL);
}


static void
gpa_add_printer (const gchar *printer_name,
		 GpaModelInfo *model_info,
		 GpaWindowInfo *wi)
{
	GpaPrinter *printer;
	
	debug (FALSE, "");

	g_return_if_fail (printer_name != NULL);
	g_return_if_fail (strlen (printer_name) != 0);
	g_return_if_fail (model_info != NULL);
	g_return_if_fail (GNOME_IS_DIALOG (wi->dialog));

	printer = gpa_printer_new_from_model_info (model_info, printer_name);

	if (GPA_IS_PRINTER (printer)) {
		*wi->printers = g_list_append (*wi->printers, printer);
		/* This ensures that there is a default printer */
		gpa_printer_get_default (*wi->printers);
	}

	gpa_window_update (wi);
}


static void
gpa_add_dialog_button_clicked_add (GpaAddDialogInfo *adi)
{
	const gchar *id;
	GpaModelInfo *model_info;
	gchar *printer_name;
	gint row;

	debug (FALSE, "");

	g_return_if_fail (GNOME_IS_DIALOG (adi->dialog));
	g_return_if_fail (GTK_IS_CLIST(adi->models_clist));
	g_return_if_fail (GTK_IS_ENTRY(adi->name_entry));

	if (g_list_length (adi->models_clist->selection) != 1) {
		gpa_error ("lenght not equal to 1\n");
		return;
	}
	row = GPOINTER_TO_INT (adi->models_clist->selection->data);

	printer_name = g_strdup (gtk_entry_get_text (GTK_ENTRY(adi->name_entry)));
	if ((printer_name == NULL) || (*printer_name == 0)) {
		/* FIXME: this should be presented to the user in a dialog */
		gpa_error ("Please provide a printer name\n");
		return;
	}

	model_info = gtk_clist_get_row_data (adi->models_clist, row);
	id = gpa_model_info_get_id (model_info);
	
	if ((id == NULL) || (*id == 0)) {
		gpa_error ("Invalid model id\n");
		return;
	}

	gpa_add_printer (printer_name, model_info, adi->wi);
	gpa_add_dialog_hide (adi->dialog, NULL);

	g_free (printer_name);
	
}

static gboolean
gpa_add_dialog_button_clicked_ppd (GpaAddDialogInfo *adi)
{
	GpaModelInfo *model_info;
	GpaModel *model;
	gchar *file_name;
	
	debug (FALSE, "");

	g_return_val_if_fail (GNOME_IS_DIALOG (adi->dialog), FALSE);

	gpa_add_dialog_hide (adi->dialog, NULL);

	file_name = gpa_file_dialog (_("Select PPD file"), FALSE);

	g_print ("Getting ppd from file [%s]\n", file_name);

	model = gpa_ppd_add_model (file_name);

	if (model == NULL)
		return FALSE;

	/* Create the model info and add it to the list */
	model_info = gpa_model_info_new (gpa_model_get_name (model),
					 gpa_model_get_id (model),
					 gpa_model_get_vendor (model));
		
	gpa_add_printer ("Printer from PPD", model_info, adi->wi);

	return TRUE;
}

static void
gpa_add_dialog_button_clicked (GtkWidget *dialog, gint button, GpaAddDialogInfo *adi)
{
	
	debug (FALSE, "");

	g_return_if_fail (GNOME_IS_DIALOG (dialog));
	g_return_if_fail (GNOME_IS_DIALOG (adi->dialog));
	
	switch (button) {
	case 0:
		gpa_add_dialog_button_clicked_ppd (adi);
		break;
	case 1:
		gpa_add_dialog_button_clicked_add (adi);
		break;
	case 2:
		gpa_add_dialog_hide (adi->dialog, NULL);
		break;
	}
}

static void
gpa_add_dialog_append_vendor (GpaAddDialogInfo *adi,
			      GpaVendor *vendor)
{
	gchar * name[2];
	gint row;
	
	debug (FALSE, "");

	name [0] = gpa_vendor_dup_name (vendor);
	name [1] = NULL;
	
	row = gtk_clist_append (adi->vendors_clist, name);
	gtk_clist_set_row_data (adi->vendors_clist, row, vendor);

	g_free (name[0]);
	
}

static void
gpa_add_dialog_append_model (GpaAddDialogInfo *adi, GpaModelInfo *model)
{
	gchar * name[2];
	gint row;
	
	debug (FALSE, "");

	g_return_if_fail (adi->models_clist);
	g_return_if_fail (model != NULL);

	name [0] = gpa_model_info_dup_name (model);
	name [1] = NULL;
	
	row = gtk_clist_append (adi->models_clist, name);
	gtk_clist_set_row_data (adi->models_clist, row, model);

	g_free (name[0]);
	
}

static gboolean
gpa_add_dialog_populate_vendors (GpaAddDialogInfo *adi)
{
	GList *vendors_list;
	GList *list;

	debug (FALSE, "");

	vendors_list = gpa_vendor_list_get ();

	g_return_val_if_fail (vendors_list   != NULL, FALSE);
	g_return_val_if_fail (adi->vendors_clist  != NULL, FALSE);

	gtk_clist_clear (adi->vendors_clist);
	gtk_clist_clear (adi->models_clist);
	list = vendors_list;

	for ( ; list != NULL; list = list->next)
		gpa_add_dialog_append_vendor (adi, (GpaVendor *) list->data);
	
	return TRUE;
}


static void
gpa_add_dialog_populate_models (GpaAddDialogInfo *adi, GpaVendor *vendor)
{
	GList *list;

	debug (FALSE, "");

	g_return_if_fail (vendor != NULL);
	
	list = gpa_vendor_get_models_list (vendor);

	gtk_clist_clear (adi->models_clist);
	
	for ( ; list != NULL; list = list->next)
		gpa_add_dialog_append_model (adi,
					     (GpaModelInfo *) list->data);
	
}

static void
gpa_add_dialog_update_printer_name (GpaAddDialogInfo *adi, gint row)
{
	GpaModelInfo *model_info;
 	gchar *text;

	debug (FALSE, "");

	if (g_list_length (adi->models_clist->selection) != 1)
		return;

	model_info = gtk_clist_get_row_data (adi->models_clist, row);
#if 0
	text = gpa_model_info_dup_name (model_info);
#endif
#if 0	
	text = g_strdup_printf ("My %s %s Printer",
				model->vendor->name,
				model->name);
#endif
#if 1
	text = g_strdup_printf ("%s",
				gpa_model_info_get_name (model_info));
#endif	
	gtk_entry_set_text (adi->name_entry, text);
	g_free (text);
}

static void
gpa_add_dialog_model_clicked (GtkCList *models, GdkEventButton *event, GpaAddDialogInfo *adi)
{
	gint row;
	gint column;
	
	debug (FALSE, "");

	if (event->button != 1)
		return;
	if (event->type != GDK_BUTTON_PRESS)
		return;
	if (!gtk_clist_get_selection_info (models, event->x, event->y, &row, &column))
		return;

	gpa_add_dialog_update_printer_name (adi, row);
}
	


static void
gpa_add_dialog_vendor_clicked (GtkCList *clist, GdkEventButton *event, GpaAddDialogInfo *adi)
{
	GpaVendor *vendor;
	GtkWidget *dialog;
	gint row;
	gint column;
	
	debug (FALSE, "");

	if (event->button != 1)
		return;
	if (event->type != GDK_BUTTON_PRESS)
		return;
	if (!gtk_clist_get_selection_info (clist, event->x, event->y, &row, &column))
		return;
	
	dialog = GTK_WIDGET (adi->dialog);
	g_return_if_fail (GNOME_IS_DIALOG (dialog));

	vendor = gtk_clist_get_row_data (clist, row);
	gpa_add_dialog_populate_models (adi, vendor);

	gpa_add_dialog_update_printer_name (adi, 0);
}

static gboolean
gpa_add_init (GpaAddDialogInfo *adi)
{
	GladeXML *gui;
	GtkWidget *add_dialog;
	GtkWidget *vendors_clist;
	GtkWidget *models_clist;
	GtkWidget *printer_name_entry;

	debug (FALSE, "");

	g_return_val_if_fail (GNOME_IS_DIALOG (adi->wi->dialog), FALSE);
	g_return_val_if_fail (GLADE_IS_XML (adi->wi->gui), FALSE);

	gui = adi->wi->gui;
	add_dialog         = glade_xml_get_widget (gui, "add_dialog");
	vendors_clist      = glade_xml_get_widget (gui, "vendors_list");
	models_clist       = glade_xml_get_widget (gui, "models_list");
	printer_name_entry = glade_xml_get_widget (gui, "printer_name_entry");

	if (add_dialog == NULL)
		g_print ("Is null\n");
	if (!GNOME_IS_DIALOG (add_dialog)) {
		gpa_error ("add_dialog type error, expected gnome-dialog");
		return FALSE;
	}
	if (!GTK_IS_CLIST (models_clist)) {
		gpa_error ("models_list type error, expected clist");
		return FALSE;
	}
	if (!GTK_IS_CLIST (vendors_clist)) {
		gpa_error ("vendors type error, expected clist");
		return FALSE;
	}
	if (!GTK_IS_ENTRY (printer_name_entry)) {
		gpa_error ("printer_name_entry type error, expected gtk_entry");
		return FALSE;
	}

	/* connect signals */
	gtk_signal_connect (GTK_OBJECT (add_dialog), "destroy",
			    GTK_SIGNAL_FUNC (gpa_add_dialog_hide),
			    NULL);
	gtk_signal_connect (GTK_OBJECT (add_dialog), "delete_event",
			    GTK_SIGNAL_FUNC (gpa_add_dialog_hide_by_event),
			    NULL);
	gtk_signal_connect (GTK_OBJECT (add_dialog), "clicked",
			    GTK_SIGNAL_FUNC (gpa_add_dialog_button_clicked),
			    adi);

	gnome_dialog_close_hides (GNOME_DIALOG (add_dialog), TRUE);

	/* Conect the signal for the manufacturers list, so that we can
	   display the available printers for each manufacturer */
	gtk_signal_connect (GTK_OBJECT (vendors_clist), "button_press_event",
			    GTK_SIGNAL_FUNC (gpa_add_dialog_vendor_clicked),
			    adi);
	gtk_signal_connect (GTK_OBJECT (models_clist), "button_press_event",
			    GTK_SIGNAL_FUNC (gpa_add_dialog_model_clicked),
			    adi);

	adi->dialog        = add_dialog;
	adi->vendors_clist = GTK_CLIST (vendors_clist);
	adi->models_clist  = GTK_CLIST (models_clist);
	adi->name_entry    = GTK_ENTRY (printer_name_entry);

	
	debug (FALSE, "end");
	
	return TRUE;
}

/**
 * gpa_add_dialog:
 * @wi: 
 * 
 * Creates and displays the Add Printer dialog
 * 
 * Return Value: 
 **/
gboolean
gpa_add_dialog (GpaWindowInfo *wi)
{
	GpaAddDialogInfo *adi = NULL;

	if (wi->adi == NULL) {
		adi = g_new (GpaAddDialogInfo, 1);
		adi->wi = wi;
		adi->dialog = NULL;
		wi->adi = adi;

		if (!gpa_add_init (adi))
			return FALSE;

	}

	gpa_add_dialog_populate_vendors (wi->adi);
	
	g_return_val_if_fail (GNOME_IS_DIALOG (wi->adi->dialog), FALSE);

	gtk_widget_show_all (wi->adi->dialog);

	return TRUE;
}
