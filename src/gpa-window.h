#ifndef __GNOME_PRINT_ADMIN_WINDOW_H__
#define __GNOME_PRINT_ADMIN_WINDOW_H__

BEGIN_GNOME_DECLS

extern gboolean   debug_turned_on;

#include <gtk/gtkwidget.h>
#include <gtk/gtkclist.h>
#include <libgpa/gpa-structs.h>

typedef struct _GpaWindowInfo GpaWindowInfo;

#include "gpa-add.h"

struct _GpaWindowInfo{
	GladeXML *gui;
	GtkWidget *dialog;

	GtkCList *installed_clist;

	GList **printers;

	/* Buttons */
	GtkWidget *ok_button;
	GtkWidget *cancel_button;
	
	GtkWidget *add_button;
	GtkWidget *remove_button;
	GtkWidget *configure_button;
	GtkWidget *default_button;
	GtkWidget *emit_button; /* Remove */

	GpaAddDialogInfo *adi;
};


gboolean gpa_window_create (GList **printers);

void     gpa_window_set_sensitivity (GpaWindowInfo *wi);
void     gpa_window_update (GpaWindowInfo *wi);

END_GNOME_DECLS

#endif /* __GNOME_PRINT_ADMIN_WINDOW_H__ */

