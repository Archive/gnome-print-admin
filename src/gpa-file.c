/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * gpa-file.c: A file selector to use in print to file 
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Authors :
 *   Chema Celorio (chema@celorio.com)
 *
 */



#include "config.h"
#include <gnome.h>
#include "gpa-file.h"

#define GPA_FILE_SELECTOR_FILENAME "GpaFileSelectorFileName"
#define GPA_FILE_SELECTOR_WARN "GpaFileSelectorWarn"

static void 
gpa_file_destroy_cb (gpointer dummy, gpointer widget)
{
	gtk_widget_destroy (GTK_WIDGET (widget));
	gtk_main_quit ();
}

static void
gpa_file_ok_selected (gpointer a, GtkFileSelection *f)
{
	gchar *file_name;
	gchar **file_name_;
	gchar *warn;

	file_name_ = gtk_object_get_data (GTK_OBJECT (f), GPA_FILE_SELECTOR_FILENAME);
	warn       = gtk_object_get_data (GTK_OBJECT (f), GPA_FILE_SELECTOR_WARN);

	g_return_if_fail (file_name_ != NULL);
	
	file_name = g_strdup (gtk_file_selection_get_filename (f));

	if (file_name[0] == 0) {
		g_free (file_name);
		file_name = NULL;
		*file_name_ = file_name;
		return;
	}
	
	if (g_file_test (file_name, G_FILE_TEST_ISDIR)) {
		g_free (file_name);
		file_name = NULL;
		*file_name_ = file_name;
		return;
	}

	if (warn != NULL && g_file_exists (file_name))
	{
		guchar * msg;
		GtkWidget *msgbox;
		gint ret;
		g_free (warn);
		msg = g_strdup_printf (_("``%s'' is about to be overwritten.\n"
					 "Do you want to continue ?"), file_name);
		msgbox = gnome_message_box_new (msg,
						GNOME_MESSAGE_BOX_QUESTION,
						GNOME_STOCK_BUTTON_YES,
						GNOME_STOCK_BUTTON_NO,
						GNOME_STOCK_BUTTON_CANCEL,
						NULL);
		gnome_dialog_set_default (GNOME_DIALOG (msgbox), 2);
		ret = gnome_dialog_run_and_close (GNOME_DIALOG (msgbox));
		g_free (msg);
		switch (ret)
		{
		/* Yes selected */
		case 0:
			break;
		case 1:
			return;
		default:
			g_free (file_name);
			file_name = NULL;
			gpa_file_destroy_cb (NULL, GTK_WIDGET (f));
			return;
		}
	}

	*file_name_ = file_name;
	
	gpa_file_destroy_cb (NULL, f);
}

static gint
gpa_file_dialog_key (GtkFileSelection *fsel, GdkEventKey *event)
{
	if (event->keyval == GDK_Escape) {
		gtk_button_clicked (GTK_BUTTON (fsel->cancel_button));
		return 1;
	}

	return 0;
}


static void 
gpa_file_delete_event_cb (gpointer dummy, gpointer dummy2, gpointer widget)
{
	gpa_file_destroy_cb (NULL, widget);
}

static GtkWidget *
gpa_file_create (gchar **filename, gboolean warn_overwrite)
{
	GtkWidget *file_selector;

	file_selector = gtk_file_selection_new (NULL);

	gtk_object_set_data (GTK_OBJECT (file_selector), GPA_FILE_SELECTOR_FILENAME, filename);
	if (warn_overwrite)
		gtk_object_set_data (GTK_OBJECT (file_selector), GPA_FILE_SELECTOR_WARN, "TRUE");
	
	gtk_signal_connect (GTK_OBJECT (file_selector),
			    "delete_event",
			    GTK_SIGNAL_FUNC (gpa_file_delete_event_cb),
			    file_selector);
	gtk_signal_connect (GTK_OBJECT (file_selector),
			    "key_press_event",
			    GTK_SIGNAL_FUNC (gpa_file_dialog_key),
			    NULL);
	gtk_signal_connect (GTK_OBJECT (GTK_FILE_SELECTION(file_selector)->cancel_button),
			    "clicked",
			    GTK_SIGNAL_FUNC (gpa_file_destroy_cb),
			    file_selector);
	gtk_signal_connect (GTK_OBJECT(GTK_FILE_SELECTION (file_selector)->ok_button),
			    "clicked",
			    GTK_SIGNAL_FUNC (gpa_file_ok_selected),
			    file_selector);

	return file_selector;
}


/**
 * gpa_file_dialog:
 * @printer: 
 * 
 * 
 * 
 * Return Value: -1 on user canceled or erorr, 0 otherwise
 **/
gchar *
gpa_file_dialog (const gchar *message, gboolean warn_overwrite)
{
	GtkWidget *file_selector;
	gchar *filename = NULL;

	file_selector = gpa_file_create (&filename, warn_overwrite);
	
	gtk_window_set_modal (GTK_WINDOW (file_selector), TRUE);
	gtk_window_set_title (GTK_WINDOW (file_selector), message);

	gtk_widget_show (file_selector);

	gtk_main ();
	
	return filename;
}
