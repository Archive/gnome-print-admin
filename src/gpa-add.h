#ifndef __GNOME_PRINT_ADMIN_ADD_H__
#define __GNOME_PRINT_ADMIN_ADD_H__

BEGIN_GNOME_DECLS

extern gboolean debug_turned_on;

#include <gtk/gtkentry.h>

typedef struct _GpaAddDialogInfo GpaAddDialogInfo;

struct _GpaAddDialogInfo{
	GpaWindowInfo *wi;

	GtkWidget *dialog;

	GtkCList *vendors_clist;
	GtkCList *models_clist;
	GtkEntry *name_entry;
};


gboolean gpa_add_dialog (GpaWindowInfo *wi);

END_GNOME_DECLS

#endif /* __GNOME_PRINT_ADMIN_ADD_H__ */

