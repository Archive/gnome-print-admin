/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Copyright (C) 2000 Jose M Celorio
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Authors :
 *  Chema Celorio <chema@celorio.com>
 *
 */

#include "gpa-defs.h"

#include <glib.h>
#include <gtk/gtkwidget.h>
#include <gtk/gtksignal.h>
#include <gtk/gtkmain.h>
#include <gtk/gtkbutton.h>
#include <gtk/gtkclist.h>
#include <glade/glade-xml.h>
#include <libgnome/gnome-util.h>
#include <libgnomeui/gnome-dialog.h>

#include <dirent.h> /* For the DIR structure stuff */
#include <stdio.h>  /* For remove () */

#include "gpa-window.h"
#include "gpa-add.h"

#include <libgpa/gpa-printer.h>
#include <libgpa/gpa-code.h>
#include <libgpa/gpa-vendor.h>
#include <libgpa/gpa-model.h>
#include <libgpaui/gpa-config.h>

	
static void
gpa_window_destroy (GtkWidget *widget, GpaWindowInfo *wi)
{
	debug (FALSE, "");
	
	g_return_if_fail (GNOME_IS_DIALOG (wi->dialog));

	gtk_widget_destroy (wi->dialog);
	gtk_main_quit();
}

static GpaPrinter *
gpa_window_get_selected_printer (GpaWindowInfo *wi)
{
	GpaPrinter *printer;
	gint row;
	
	g_return_val_if_fail (GNOME_IS_DIALOG (wi->dialog), NULL);

	if (g_list_length (wi->installed_clist->selection) != 1)
		return NULL;

	if (g_list_length (wi->installed_clist->row_list) < 1)
		return NULL;

	row = GPOINTER_TO_INT (wi->installed_clist->selection->data);
	printer = gtk_clist_get_row_data (wi->installed_clist, row);

	if (!GPA_IS_PRINTER (printer))
		return NULL;
	
	return printer;
}

static gint
gpa_window_get_selected_row (GpaWindowInfo *wi)
{
	gint row;
	
	g_return_val_if_fail (GNOME_IS_DIALOG (wi->dialog), -1);
	
	if (g_list_length (wi->installed_clist->selection) != 1)
		return -1;

	row = GPOINTER_TO_INT (wi->installed_clist->selection->data);

	return row;
}


void
gpa_window_set_sensitivity (GpaWindowInfo *wi)
{
	const GpaPrinter *printer;
	gboolean selection;
	
	g_return_if_fail (GNOME_IS_DIALOG (wi->dialog));

	if (g_list_length (wi->installed_clist->selection) == 0)
		selection = FALSE;
	else
		selection = TRUE;

	printer = gpa_window_get_selected_printer (wi);
	
	gtk_widget_set_sensitive (wi->remove_button, selection);
	gtk_widget_set_sensitive (wi->configure_button, selection);
	gtk_widget_set_sensitive (wi->default_button, printer && !gpa_printer_is_default (printer));
	gtk_widget_set_sensitive (wi->emit_button, selection);

}

static void
gpa_window_set_sensitivity_cb (GtkObject *dummy,
			       gint row,
			       gint col,
			       GdkEvent *no_used,
			       GpaWindowInfo *wi)
{
	g_return_if_fail (wi != NULL);
	g_return_if_fail (GNOME_IS_DIALOG (wi->dialog));

	gpa_window_set_sensitivity (wi);
}


static void
gpa_window_remove_printer (GpaWindowInfo *wi)
{
	GpaPrinter *printer;
	
	g_return_if_fail (GNOME_IS_DIALOG (wi->dialog));
	g_return_if_fail (GTK_IS_CLIST (wi->installed_clist));

	printer = gpa_window_get_selected_printer (wi);

	if (printer == NULL) {
		g_warning ("The remove button should have been unsensitive\n"
			   "Or the clist is not in BROWSE mode\n");
		return;
	}

	*wi->printers = g_list_remove (*wi->printers, printer);
	gpa_printer_unref (printer);

	gpa_window_update (wi);
}


static gint
g_str_compare (gconstpointer v, gconstpointer v2)
{
  return strcmp ((const gchar*) v, (const gchar*)v2);
}

static void
gpa_delete_printers_from_dir (const char *dirname, GList *ids_list)
{
	DIR *dir;
	gchar *file_name;
	gchar *id;
	struct dirent *dent;
	const int expected_length = 40;
	
	dir = opendir (dirname);
	if (!dir)
		return;

	while ((dent = readdir (dir)) != NULL){
		int len = strlen (dent->d_name);

		if (len != expected_length)
			continue;

		if (strcmp (dent->d_name + 32, ".printer"))
			continue;

		id = g_strndup (dent->d_name ,32);

		if (g_list_find_custom (ids_list, id, g_str_compare) != NULL)
			continue;

		file_name = g_concat_dir_and_file (dirname, dent->d_name);

		remove (file_name);

		g_free (file_name);
	}
	closedir (dir);

}

static void
gpa_window_save_and_exit (GpaWindowInfo *wi)
{
	GpaPrinter *printer;
	GList *ids_list = NULL;
	GList *list;
	gchar *user_dir;
	gint rows;
	gint n;

	g_return_if_fail (GNOME_IS_DIALOG (wi->dialog));
	g_return_if_fail (GTK_IS_CLIST (wi->installed_clist));

	/* Save each printer in the lists of printers */
	rows = g_list_length (wi->installed_clist->row_list);
	for (n = 0; n < rows; n++) {
		printer = gtk_clist_get_row_data (wi->installed_clist, n);
		gpa_printer_save (printer);
		ids_list = g_list_prepend (ids_list, gpa_printer_dup_id (printer));
	}

	/* Delete the printers that are not in the ids_list */
	user_dir = gnome_util_home_file ("printers");
	gpa_delete_printers_from_dir (user_dir, ids_list);
	g_free (user_dir);

	/* Free the temp list of Id's */
	for (list = ids_list; list != NULL; list = list->next) {
		gchar *id;
		id = (gchar *) list->data;
		g_free (id);
	}
	g_list_free (list);

	gpa_window_destroy (NULL, wi);
}

static gboolean
gpa_window_populate_printers_element (GpaWindowInfo *wi, GpaPrinter *printer)
{
	const GpaVendor *vendor;
	const GpaModel *model;
	gchar *name[5];
	gint row;

	g_return_val_if_fail (GPA_IS_PRINTER (printer), FALSE);
	g_return_val_if_fail (GNOME_IS_DIALOG (wi->dialog), FALSE);

	model = gpa_printer_get_model (printer);
	g_return_val_if_fail (GPA_IS_MODEL (model), FALSE);
	
	vendor = gpa_model_get_vendor (model);
	g_return_val_if_fail (GPA_IS_VENDOR (vendor), FALSE);
	
	
	name [0] = gpa_printer_dup_name (printer);
	name [1] = gpa_printer_is_default (printer) ? "*" : "";
	name [2] = gpa_vendor_dup_name (vendor);
	name [3] = gpa_model_dup_name (model);
	name [4] = NULL;
	
	row = gtk_clist_append (wi->installed_clist,
				name);
	gtk_clist_set_row_data (wi->installed_clist,
				row, printer);

	if (name [0]) g_free (name [0]);
	/* Don't fre name [1] */
	if (name [2]) g_free (name [2]);
	if (name [3]) g_free (name [3]);	

	return TRUE;
}

static gboolean
gpa_window_populate_printers (GpaWindowInfo *wi)
{
	GpaPrinter *printer;
	GList *list;
	gint row;

	debug (FALSE, "");

	list = *wi->printers;

	gtk_clist_clear (wi->installed_clist);
	
	if (list == NULL)
		return TRUE;

	row = gpa_window_get_selected_row (wi);

	for (; list != NULL; list = list->next) {
		printer = GPA_PRINTER (list->data);
		
		if (!GPA_IS_PRINTER (printer))
			return FALSE;

		if (!gpa_window_populate_printers_element (wi, printer))
			return FALSE;

	}

	gpa_window_set_sensitivity (wi);

	if (row > -1)
		gtk_clist_select_row (wi->installed_clist, row, -1);

	return TRUE;
}

void
gpa_window_update (GpaWindowInfo *wi)
{
	g_return_if_fail (GNOME_IS_DIALOG (wi->dialog));

	gpa_window_populate_printers (wi);
	gpa_window_set_sensitivity (wi);
}


static void
gpa_window_set_default (GpaWindowInfo *wi)
{
	GpaPrinter *printer;

	g_return_if_fail (GNOME_IS_DIALOG (wi->dialog));
	g_return_if_fail (GTK_IS_CLIST (wi->installed_clist));

	printer = gpa_window_get_selected_printer (wi);

	g_return_if_fail (GPA_IS_PRINTER (printer));

	gpa_printer_set_default (*wi->printers, printer);

	gpa_window_populate_printers (wi);
}

static void
gpa_window_button_clicked (GtkWidget *button, GpaWindowInfo *wi)
{
	GpaPrinter *printer;

	g_return_if_fail (GNOME_IS_DIALOG (wi->dialog));
	g_return_if_fail (GTK_IS_BUTTON (button));

	printer = gpa_window_get_selected_printer (wi);

	if (button == wi->emit_button) 
#if 0
		gpa_code_emit_all (printer->settings_list->data);
#else
		g_warning ("Implement me !. gpa-window.c L:218\n");
#endif
	else if (button == wi->cancel_button) 
		gpa_window_destroy (NULL, wi);
	else if (button == wi->ok_button) 
		gpa_window_save_and_exit (wi);
	else if (button == wi->configure_button) 
		gpa_config_printer (printer, FALSE);
	else if (button == wi->remove_button) 
		gpa_window_remove_printer (wi);
	else if (button == wi->add_button) 
		gpa_add_dialog (wi);
	else if (button == wi->default_button) 
		gpa_window_set_default (wi);
	else 
		g_warning ("action for button is not defined\n");
	

}

static GtkWidget *
gpa_window_load_button (GpaWindowInfo *wi, const gchar *name, gint gnome_dialog_button)
{
	GtkWidget *button;

	button = glade_xml_get_widget (wi->gui, name);

	g_return_val_if_fail (GTK_IS_BUTTON (button), NULL);
	
	if (gnome_dialog_button > -1)
		gnome_dialog_button_connect (GNOME_DIALOG (wi->dialog),
					     gnome_dialog_button,
					     GTK_SIGNAL_FUNC (gpa_window_button_clicked),
					     wi);
	else
		gtk_signal_connect (GTK_OBJECT (button), "clicked",
				    GTK_SIGNAL_FUNC (gpa_window_button_clicked), wi);

	return button;
}
	

static gboolean
gpa_window_init (GpaWindowInfo *wi)
{
	GladeXML *gui;
	GtkWidget *gpa_window;
	GtkWidget *installed_clist;

	debug (FALSE, "");

	wi->dialog = NULL;
	wi->remove_button = NULL;
	wi->configure_button = NULL;
	
	if (!g_file_exists (GPA_GLADE_DIR GPA_GLADE_FILE)) {
		gpa_error ("Could not find %s",
			   GPA_GLADE_DIR GPA_GLADE_FILE);
		return FALSE;
	}

	gui = glade_xml_new (GPA_GLADE_DIR GPA_GLADE_FILE,
			     NULL);
	wi->gui = gui;
	if (wi->gui == NULL) {
		gpa_error ("Could not find " GPA_GLADE_DIR);
		return FALSE;
	}

	gpa_window        = glade_xml_get_widget (gui, "printer_manager");
	installed_clist   = glade_xml_get_widget (gui, "installed_clist");

	if (!GNOME_IS_DIALOG (gpa_window)) {
		gpa_error ("printer_manager type error, expected gnome-dialog");
		return FALSE;
	}
	if (!GTK_IS_CLIST (installed_clist)) {
		gpa_error ("installed_clist type error, expected gtk_clist");
		return FALSE;
	}
	wi->dialog           = gpa_window;
	wi->installed_clist  = GTK_CLIST (installed_clist);

	wi->ok_button        = gpa_window_load_button (wi, "ok_button", 0);
	wi->cancel_button    = gpa_window_load_button (wi, "cancel_button", -1);
	wi->add_button       = gpa_window_load_button (wi, "add_button", -1);
	wi->remove_button    = gpa_window_load_button (wi, "remove_button", -1);
	wi->configure_button = gpa_window_load_button (wi, "configure_button", -1);
	wi->default_button   = gpa_window_load_button (wi, "default_button", -1);
	wi->emit_button      = gpa_window_load_button (wi, "emit_button", -1);

	if (!wi->ok_button  ||
	    !wi->cancel_button ||
	    !wi->add_button ||
	    !wi->remove_button ||
	    !wi->configure_button ||
	    !wi->default_button ||
	    !wi->emit_button) {
		gpa_error ("Button type error, expected gtk_button");
		return FALSE;
	}
	
	/* connect signals */
	gtk_signal_connect (GTK_OBJECT (gpa_window), "destroy",
			    GTK_SIGNAL_FUNC (gpa_window_destroy), wi);
	gtk_signal_connect (GTK_OBJECT (gpa_window), "delete_event",
			    GTK_SIGNAL_FUNC (gtk_false), NULL);
	gtk_signal_connect (GTK_OBJECT (wi->installed_clist), "select_row",
			    GTK_SIGNAL_FUNC (gpa_window_set_sensitivity_cb), wi);
	gtk_signal_connect (GTK_OBJECT (wi->installed_clist), "unselect_row",
			    GTK_SIGNAL_FUNC (gpa_window_set_sensitivity_cb), wi);


#if 0
	/* FIXME: Don't unref the gui untill we destroy the dialog
	 * so that we can pull the other widgets from this gui,
	 * like the add dialog
	 */
	gtk_object_unref (GTK_OBJECT (gui));
	wi->gui = NULL;
#endif


	gpa_window_populate_printers (wi);

	gpa_window_set_sensitivity (wi);

	return TRUE;
}


gboolean
gpa_window_create (GList **printers)
{
	GpaWindowInfo *wi;

	wi = g_new (GpaWindowInfo, 1);
	wi->printers = printers;
	wi->dialog = NULL;
	wi->adi = NULL;

	if (!gpa_window_init (wi))
		return FALSE;

	if (wi->dialog == NULL)
		return FALSE;
	else
		gtk_widget_show_all (wi->dialog);

	return TRUE;
}
