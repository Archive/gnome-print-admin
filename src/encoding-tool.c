/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Copyright (C) 2000 Jose M Celorio
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Authors :
 *  Chema Celorio <chema@celorio.com>
 *
 */

#include "gpa-defs.h"


#include <glib.h>
#include <gtk/gtkwidget.h>
#include <gtk/gtkmain.h>
#include <gtk/gtksignal.h>
#include <gtk/gtkeditable.h>
#include <gtk/gtkentry.h>
#include <gtk/gtktext.h>
#include <gtk/gtkmenu.h>
#include <gtk/gtkoptionmenu.h>
#include <libgnome/gnome-popt.h>
#include <libgnome/gnome-i18n.h>
#include <libgnome/gnome-util.h>
#include <libgnomeui/gnome-init.h>
#include <libgnomeui/gnome-app.h>
#include <libgnomeprint/gnome-print-encode.h>
#include <glade/glade.h>

#define ASCII85_GLADE_FILE "encoding-tool.glade"

gboolean  debug_turned_on;
GtkWidget *main_window;
GtkWidget *encode_text;
GtkWidget *decode_text;
GtkWidget *encode_entry;
GtkWidget *decode_entry;
GtkWidget *encoding_menu;

static void
window_destroy (GtkWidget *widget, gpointer data)
{
	debug (FALSE, "");
	
	gtk_widget_destroy (main_window);
	main_window = NULL;
	gtk_main_quit();
}

void exit_cb (void);

void
exit_cb (void)
{
	window_destroy (main_window, NULL);
}

static void
update_entries (gint encoded_length, gint decoded_length)
{
	gchar *encoded_text;
	gchar *decoded_text;
	
	encoded_text = g_strdup_printf ("%i", encoded_length);
	decoded_text = g_strdup_printf ("%i", decoded_length);

	gtk_entry_set_text (GTK_ENTRY (encode_entry), encoded_text);
	gtk_entry_set_text (GTK_ENTRY (decode_entry), decoded_text);

	g_free (encoded_text);
	g_free (decoded_text);
}

static gint
gtk_menu_get_active_index (GtkMenu *menu)
{
	GtkWidget *child;
	GList *children;
	gint index = 0;
  
	g_return_val_if_fail (menu != NULL, -1);
	g_return_val_if_fail (GTK_IS_MENU (menu), -1);
  
	child = NULL;
	children = GTK_MENU_SHELL (menu)->children;

	while (children)
	{
		child = children->data;
		children = children->next;
		if (GTK_BIN (child)->child)
			break;
		index++;
		child = NULL;
	}
  
	return index;
}

static void
encode (void)
{
	GtkMenu *menu;
	gchar *encode_buffer;
	gchar *decode_buffer;
	gchar *buffer;
	gint encode_length;
	gint decode_length;
	gint length;
	gint encoding_type;

	/* We get the chars from the decoded text buffer */
	encode_buffer = gtk_editable_get_chars ( GTK_EDITABLE ( encode_text ),
						 0, -1);
	decode_buffer = gtk_editable_get_chars ( GTK_EDITABLE ( decode_text ),
						 0, -1);
	
	encode_length = strlen (encode_buffer);
	decode_length = strlen (decode_buffer);

	/* then we clear the encoded buffer */
	gtk_text_set_point (GTK_TEXT (decode_text), 0);
	gtk_text_forward_delete (GTK_TEXT (decode_text),
				 decode_length);


	menu = GTK_MENU ( GTK_OPTION_MENU (encoding_menu)->menu);
	encoding_type = gtk_menu_get_active_index (menu);

	if (encode_length == 0) {
		buffer = g_strdup ("Nothing to encode");
		length = strlen (buffer);
	}
	else
	{
		switch (encoding_type) {
		case  0:
			/* Convert to hex */
			length = gnome_print_encode_ascii85_wcs (encode_length);
			buffer = g_new (gchar, length);
			length = gnome_print_encode_ascii85 (encode_buffer,
							     buffer,
							     encode_length);
			break;
		case 1:
			length = gnome_print_encode_hex_wcs (encode_length);
			buffer = g_new (gchar, length);
			length = gnome_print_encode_hex (encode_buffer,
							 buffer,
							 encode_length);
			break;
		default:
			g_warning ("Unknown encoding type");
			return;
		}
	}


	/* Add it to the text buffer */
	gtk_text_insert (GTK_TEXT (decode_text),
			 NULL,
			 NULL,
			 NULL,
			 buffer,
			 length);
	
	update_entries (encode_length, length);

	/* Free everything */
	g_free (encode_buffer);
	g_free (decode_buffer);
	g_free (buffer);
}

static void
decode (void)
{
	GtkMenu *menu;
	gchar *encode_buffer;
	gchar *decode_buffer;
	gchar *decode_buffer_clean;
	gchar *buffer;
	gint encode_length;
	gint decode_length;
	gint encoding_type;
	gint length;
	gint n;
	gint m;

	/* We get the chars from the decoded text buffer */
	encode_buffer = gtk_editable_get_chars ( GTK_EDITABLE ( encode_text ),
						 0, -1);
	decode_buffer = gtk_editable_get_chars ( GTK_EDITABLE ( decode_text ),
						 0, -1);
	
	encode_length = strlen (encode_buffer);
	decode_length = strlen (decode_buffer);

	/* then we clear the encoded buffer */
	gtk_text_set_point (GTK_TEXT (encode_text), 0);
	gtk_text_forward_delete (GTK_TEXT (encode_text),
				 encode_length);

	menu = GTK_MENU ( GTK_OPTION_MENU (encoding_menu)->menu);
	encoding_type = gtk_menu_get_active_index (menu);

	if (decode_length == 0) {
		buffer = g_strdup ("Nothing to decode\n");
		length = strlen (buffer);
	}
	else
	{
		/* clean the buffer */
		decode_buffer_clean = g_new (gchar, decode_length);
		m = 0;
		for (n=0; n<decode_length; n++) {
			if ((decode_buffer [n] != '\n') &&
			    (decode_buffer [n] != '\t') &&
			    (decode_buffer [n] != ' ') &&
			    (decode_buffer [n] != '\r'))
				decode_buffer_clean [m++] = decode_buffer [n];
		}
		switch (encoding_type) {
		case  0:
			length = gnome_print_decode_ascii85_wcs (decode_length);
			buffer = g_new (gchar, length);
			/* Convert to ascii85 */
			length = gnome_print_decode_ascii85 (decode_buffer_clean,
							     buffer, m);
			break;
		case 1:
			length = gnome_print_decode_hex_wcs (decode_length);
			buffer = g_new (gchar, length);
			/* Convert to hex */
			length = gnome_print_decode_hex (decode_buffer_clean,
							 buffer, m);
			break;
		default:
			g_warning ("Could not determine decoding type\n");
			return;
		}
	}

	/* Add it to the text buffer */
	gtk_text_insert (GTK_TEXT (encode_text),
			 NULL,
			 NULL,
			 NULL,
			 buffer,
			 length);

	update_entries (length, decode_length);

	/* Free everything */
	g_free (encode_buffer);
	g_free (decode_buffer);
	g_free (buffer);
}

static GtkWidget *
hextool_create_main_window ()
{
	GladeXML *gui_;
	GtkWidget *encode_button;
	GtkWidget *decode_button;
	GtkWidget *exit_button;

	debug (FALSE, "");
	
	if (!g_file_exists (GPA_GLADE_DIR ASCII85_GLADE_FILE)) {
		g_warning ("Could not find %s",
			   GPA_GLADE_DIR ASCII85_GLADE_FILE);
		return FALSE;
	}
	
	gui_ = glade_xml_new (GPA_GLADE_DIR ASCII85_GLADE_FILE, NULL);

	if (!gui_) {
		g_warning ("Could not find " GPA_GLADE_DIR);
		return FALSE;
	}

	main_window   = glade_xml_get_widget (gui_, "main_window");
	encode_button = glade_xml_get_widget (gui_, "encode_button");
	decode_button = glade_xml_get_widget (gui_, "decode_button");
	exit_button   = glade_xml_get_widget (gui_, "exit_button");
	encode_text   = glade_xml_get_widget (gui_, "encoded_text");
	decode_text   = glade_xml_get_widget (gui_, "decoded_text");
	encode_entry  = glade_xml_get_widget (gui_, "encoded_entry");
	decode_entry  = glade_xml_get_widget (gui_, "decoded_entry");
	encoding_menu = glade_xml_get_widget (gui_, "encoding_option_menu");
		
	if ((main_window   == NULL) ||
	    (encode_button == NULL) ||
	    (decode_button == NULL) ||
	    (encode_text   == NULL) ||
	    (decode_text   == NULL) ||
	    (encode_entry  == NULL) ||
	    (decode_entry  == NULL) ||
	    (encoding_menu == NULL) ||
	    (exit_button   == NULL)) {
		g_warning ("Could not load widgets from file %s",
			   GPA_GLADE_DIR ASCII85_GLADE_FILE);
		return NULL;
	}
	
	/* connect signals */
	gtk_signal_connect (GTK_OBJECT (main_window), "destroy",
			    GTK_SIGNAL_FUNC (window_destroy), NULL);
	gtk_signal_connect (GTK_OBJECT (main_window), "delete_event",
			    GTK_SIGNAL_FUNC (gtk_false), NULL);
	gtk_signal_connect (GTK_OBJECT (exit_button), "activate",
			    GTK_SIGNAL_FUNC (window_destroy), NULL);

	gtk_signal_connect (GTK_OBJECT (encode_button), "clicked",
			    GTK_SIGNAL_FUNC (encode), NULL);
	gtk_signal_connect (GTK_OBJECT (decode_button), "clicked",
			    GTK_SIGNAL_FUNC (decode), NULL);

	
	return main_window;
}

gboolean debug_turned_on = FALSE;

GtkWidget *main_window = NULL;

static const struct poptOption options[] =
{
	{ "debug", '\0', 0, &debug_turned_on, 0,
	  N_("Show window debugging messages."), NULL },

	{NULL, '\0', 0, NULL, 0}
};

int
main (int argc, char *argv[])
{
	char** args;			/* leftover command-line arguments */
	poptContext popt_context;	/* the popt context */
	int i;			/* argument index */
	
	/* Initialize i18n */
#if 0	/* Kill warning */
	bindtextdomain (PACKAGE, GNOMELOCALEDIR);
	textdomain (PACKAGE);
#endif	

	gnome_init_with_popt_table ("hex-tool", VERSION, argc,
				    argv, options, 0, &popt_context);

	args = (char**) poptGetArgs (popt_context);

	for (i = 0; args && args[i]; i++)
		g_print ("Argument :*%s*\n", args[i]);

	glade_gnome_init ();

	main_window  = hextool_create_main_window ();

	if (main_window != NULL) {
		gtk_widget_show_all (main_window);
		gtk_main ();
	}
	
	poptFreeContext(popt_context);
  
	return 0;
}
