#!/bin/sh
# Run this to generate all the initial makefiles, etc.

srcdir=`dirname $0`
test -z "$srcdir" && srcdir=.

PKG_NAME="Gnome Print Admin"

(test -f $srcdir/configure.in \
  && test -f $srcdir/src/gnome-print-admin.c) || {
    echo -n "**Error**: Directory "\`$srcdir\'" does not look like the"
    echo " top-level gnome-print-admin directory"
    exit 1
}

. $srcdir/macros/autogen.sh
