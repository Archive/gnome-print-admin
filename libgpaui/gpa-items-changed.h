#ifndef __GPA_OPTION_CHANGED_H__
#define __GPA_OPTION_CHANGED_H__

BEGIN_GNOME_DECLS

extern gboolean   debug_turned_on;

#include <libgpa/gpa-printer.h>
#include <glade/glade-xml.h>

void gpa_option_changed (GtkWidget *widget, GpaConfigDialogInfo *cdi);
void gpa_option_changed_numeric (GtkEditable *editable, GpaConfigDialogInfo *cdi);
void gpa_option_changed_combo_entry (GtkWidget *widget, GpaConfigDialogInfo *cdi);
void gpa_option_changed_boolean (GtkWidget *widget, GpaConfigDialogInfo *cdi);
void gpa_option_changed_update_children (GpaConfigDialogInfo *cdi,
								 GtkWidget *button,
								 GpaOption *option);

void gpa_orientation_toggled (GtkWidget *widget, GpaConfigDialogInfo *cdi);
void gpa_rotate_toggled (GtkWidget *widget, GpaConfigDialogInfo *cdi);

END_GNOME_DECLS

#endif /* __GPA_OPTION_CHANGED_H___ */


