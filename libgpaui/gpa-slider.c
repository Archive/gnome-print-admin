/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Copyright (C) 2000 Jose M Celorio
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Authors :
 *  Chema Celorio <chema@celorio.com>
 *
 */

#include "gpa-defs.h"

#include <glib.h>
#include <gtk/gtkwidget.h>
#include <gtk/gtksignal.h>
#include <gtk/gtkentry.h>
#include <gtk/gtkadjustment.h>
#include <gtk/gtkhbox.h>
#include <gtk/gtklabel.h>
#include <gtk/gtkscale.h>
#include <gtk/gtkhscale.h>
#include <libgnome/gnome-util.h>

#include "gpa-slider.h"

#include <libgpa/gpa-option.h>
#include <libgpa/gpa-options.h>
#include <libgpa/gpa-settings.h>
#include <libgpa/gpa-tags.h>

typedef struct {
	gboolean updating;
	
	GtkEntry *entry;
	GtkAdjustment *adjustment;
	GpaOption *option;
	GpaConfigDialogInfo *cdi;
}GpaSliderInfo;

static void
gpa_slider_entry_changed (GtkWidget *entry, GpaSliderInfo *si)
{
	GtkAdjustment *adjustment;
	double value;

	debug (FALSE, "");

	if (si->updating)
		return;
	
	adjustment = si->adjustment;
	
	g_return_if_fail (GTK_IS_ADJUSTMENT (adjustment));
	g_return_if_fail (GTK_IS_ENTRY (si->entry));

	value = (double) atof (gtk_entry_get_text (si->entry));

	if (adjustment->value == value)
		return;

	adjustment->value = value;

	gtk_adjustment_changed (adjustment);

}

static void
gpa_slider_adjustment_changed (GtkAdjustment *adjustment,
			       GpaSliderInfo *si)
{
	gchar *text;
	
	debug (FALSE, "");

	g_return_if_fail (GTK_IS_ENTRY (si->entry));

	if (adjustment->value >= adjustment->upper - 0.999) {
		adjustment->value = (gint) (adjustment->upper);
		text = g_strdup_printf ("%g", adjustment->value);
	} else {
		text = g_strdup_printf ("%5.1f", adjustment->value);
	}

	si->updating = TRUE;
	gtk_entry_set_text (si->entry, text);
	si->updating = FALSE;
	
	g_free (text);
}

static void
gpa_slider_option_changed (GtkAdjustment *adjustment, GpaSliderInfo *si)
{
	const GpaOptions *parent;
	GtkEntry *entry;
	gchar *new_value;
	gchar *key;

	debug (FALSE, "");

	adjustment = si->adjustment;
	entry = si->entry;
	
	g_return_if_fail (GTK_IS_ADJUSTMENT (adjustment));
	g_return_if_fail (GTK_IS_ENTRY (entry));
	g_return_if_fail (GPA_IS_SETTINGS (si->cdi->loaded_settings));
	
	new_value = g_strdup_printf ("%10.5f", adjustment->value);

	parent = gpa_option_get_parent (si->option);
	
	key = g_strdup_printf ("%s" GPA_PATH_DELIMITER "%s",
			       gpa_options_get_id (parent),
			       gpa_option_get_id (si->option));
	
	gpa_settings_value_replace (si->cdi->loaded_settings,
				    key,
				    new_value);
	
	g_free (key);
	g_free (new_value);
}


GtkWidget *
gpa_slider (GpaConfigDialogInfo *cdi,
	    GpaOption *option,
	    GtkAdjustment *adjustment,
	    GtkEntry **entry_)
{
	GpaSliderInfo *slider_info;
	GtkWidget *hbox;
	GtkWidget *label;
	GtkWidget *scale;
	GtkWidget *entry;

	debug (FALSE, "");
	
	hbox = gtk_hbox_new (FALSE, 18);
	label = gtk_label_new (gpa_option_get_name (option));
	scale = gtk_hscale_new (GTK_ADJUSTMENT (adjustment));
	entry = gtk_entry_new ();

	gtk_scale_set_draw_value (GTK_SCALE (scale), FALSE);
		
	gtk_widget_set_usize (scale, 200, 0);
	gtk_widget_set_usize (entry, 60, 0);

	slider_info = g_new (GpaSliderInfo, 1);
	slider_info->entry = GTK_ENTRY (entry);
	slider_info->adjustment = adjustment;
	slider_info->option = option;
	slider_info->cdi = cdi;
	slider_info->updating = FALSE;

	gtk_signal_connect (GTK_OBJECT (adjustment), "value_changed",
			    GTK_SIGNAL_FUNC (gpa_slider_adjustment_changed),
			    slider_info);
	gtk_signal_connect (GTK_OBJECT (entry), "changed",
			    GTK_SIGNAL_FUNC (gpa_slider_entry_changed),
			    slider_info);

	gtk_signal_connect (GTK_OBJECT (adjustment), "value_changed",
			    GTK_SIGNAL_FUNC (gpa_slider_option_changed),
			    slider_info);
	gtk_signal_connect (GTK_OBJECT (adjustment), "changed",
			    GTK_SIGNAL_FUNC (gpa_slider_option_changed),
			    slider_info);

	gtk_object_set_data (GTK_OBJECT (entry), "option", option);
	gtk_object_set_data (GTK_OBJECT (entry), "adjustment", adjustment);
	
	gtk_box_pack_start (GTK_BOX(hbox), label, FALSE, FALSE, 0);
	gtk_box_pack_start (GTK_BOX(hbox), scale, FALSE, FALSE, 0);
	gtk_box_pack_start (GTK_BOX(hbox), entry, FALSE, FALSE, 0);

	gpa_slider_adjustment_changed (adjustment, slider_info);

	*entry_ = GTK_ENTRY (entry);
	
	return hbox;
}

