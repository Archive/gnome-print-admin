/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Copyright (C) 2000 Jose M Celorio
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Authors :
 *  Chema Celorio <chema@celorio.com>
 *
 */

#include "gpa-defs.h"

#include <glib.h>

#include <gtk/gtk.h>

#include "gpa-config.h"
#include "gpa-slider.h"
#include "gpa-items-create.h"
#include "gpa-items-changed.h"

#include <libgpa/libgpa.h>


/* Prototypes for recursive function dependencies */
static GtkWidget * gpa_create_widget_from_options (GpaConfigDialogInfo *cdi,
						   GpaOptions *options,
						   GtkWidget *table);

#define GPA_CONFIG_TABLE_BORDER 10
#define GPA_CONFIG_TABLE_ROW_SPACING 10
#define GPA_CONFIG_TABLE_COL_SPACING 10
#define GPA_CONFIG_TABLE_PAD 0

#define GPA_CONFIG_PAGE_VBOX_BORDER 10
#define GPA_CONFIG_PAGE_VBOX_SPACING 15

typedef struct {
	GtkWidget *widget;
	GpaOptions *options;
}GpaConfigWidgetInfo;

static void
gpa_gtk_radio_button_select (GSList *group, int n)
{
	GSList *l;
	int len = g_slist_length (group);

	if (n >= len || n < 0)
	{
		g_warning ("Cant' set the radio button. invalid number"
			   "n %i len %i\n", n, len);
		return;
	}
	
	l = g_slist_nth (group, len - n - 1);
	gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (l->data), 1);
}



#if 0 /* Not used cause we are using gtk_combo now for options, but we might need it later */
static GtkWidget *
gpa_create_menu_item_from_option (GpaConfigDialogInfo *cdi,
				  GpaOption *option)
{
	GtkWidget *menu_item;
	gchar *name;

	debug (FALSE, "");
	
	name = gpa_option_dup_name (option);
	menu_item = gtk_menu_item_new_with_label (name);

	gtk_object_set_data (GTK_OBJECT (menu_item), "option", option);
	
	gtk_signal_connect (GTK_OBJECT (menu_item), "activate",
			    GTK_SIGNAL_FUNC (gpa_option_changed),
			    cdi);
	g_free (name);

	debug (FALSE, "false");
	
	return menu_item;
}

static GtkWidget *
gpa_create_menu_from_options (GpaConfigDialogInfo *cdi,
			      const GpaOptions *options,
			      gint *selected)
{
	GpaOption *option;
	GtkWidget *menu;
	GtkWidget *menu_item;
	GList *list;
	gint idx = 0;
	
	g_return_val_if_fail (GPA_IS_OPTIONS (options), NULL);
	g_return_val_if_fail (GPA_IS_SETTINGS (cdi->loaded_settings), NULL);

	*selected = 0;
	
	list = gpa_options_get_children (options);
	menu = gtk_menu_new ();
	for (; list != NULL; list = list->next) {
		option = (GpaOption *) list->data;
		menu_item = gpa_create_menu_item_from_option (cdi, option);
		gtk_widget_show (menu_item);
		gtk_menu_append (GTK_MENU (menu), menu_item);
		if (gpa_option_is_selected (cdi->loaded_settings, option))
			*selected = idx;
		idx++;
	}

	return menu;
}

static GtkWidget*
gpa_create_option_menu_from_options (GpaConfigDialogInfo *cdi,
				     GpaOptions *options,
				     GtkWidget *table)
{
	GtkWidget *option_menu;
	GtkWidget *menu;
	GtkWidget *hbox;
	GtkWidget *label;
	gchar *path;
	gint selected = 0;
	gint row;

	debug (FALSE, "");

	g_return_val_if_fail (GPA_IS_OPTIONS (options), NULL);
	g_return_val_if_fail (GPA_IS_SETTINGS (cdi->loaded_settings), NULL);

	menu = gpa_create_menu_from_options (cdi, options, &selected);
	option_menu = gtk_option_menu_new ();
	gtk_option_menu_set_menu    (GTK_OPTION_MENU (option_menu), menu);
	gtk_option_menu_set_history (GTK_OPTION_MENU (option_menu), selected);

	path = gpa_options_dup_path (options);
	g_hash_table_insert (cdi->widgets, path, option_menu);

	label = gtk_label_new (gpa_options_get_name (options));
	hbox = gtk_hbox_new (FALSE, 0);

	if (table != NULL) {
		/*
		  gtk_misc_set_alignment (GTK_MISC (l), 1.0, 0.5);
		*/
		row = GTK_TABLE (table)->nrows;
		gtk_table_attach (GTK_TABLE (table), label,
				  0, 1, row, row+1,
				  GTK_FILL|GTK_EXPAND, 0, GPA_CONFIG_TABLE_PAD, GPA_CONFIG_TABLE_PAD);
		gtk_table_attach (GTK_TABLE (table), option_menu,
				  1, 2, row, row+1,
				  GTK_FILL|GTK_EXPAND, 0, GPA_CONFIG_TABLE_PAD, GPA_CONFIG_TABLE_PAD);
#if 0	
		gtk_table_set_row_spacing (GTK_TABLE (table), row-1, );
#endif
	}
	
	return hbox;
}
#endif

static GtkWidget*
gpa_create_combo_only (GpaConfigDialogInfo *cdi,
		       GpaOptions *options)
{
	GpaOption *option;
	GpaOption *selected_option = NULL;
	GtkWidget *combo;
	GList *strings = NULL;
	GList *list;
	gchar *path;
	
	/* Create the lists of strings */
	list = gpa_options_get_children (options);
	for (; list != NULL; list = list->next) {
		option = (GpaOption *) list->data;
		if (gpa_option_is_selected (cdi->loaded_settings, option))
			selected_option = option;
		strings = g_list_append (strings, (gchar *)gpa_option_get_name (option));
	}
	g_return_val_if_fail (selected_option != NULL, NULL);

	/* Create the combo */
	combo = gtk_combo_new ();
	gtk_combo_set_value_in_list (GTK_COMBO (combo), TRUE, FALSE);
	gtk_combo_set_popdown_strings (GTK_COMBO (combo), strings);
	gtk_entry_set_editable (GTK_ENTRY (GTK_COMBO (combo)->entry), FALSE);
	gtk_entry_set_text (GTK_ENTRY (GTK_COMBO (combo)->entry),
			    gpa_option_get_name (selected_option));
	gtk_object_set_data (GTK_OBJECT (GTK_OBJECT (GTK_COMBO (combo)->entry)),
			     "options",
			     options);
	gtk_signal_connect (GTK_OBJECT (GTK_COMBO (combo)->entry),
			    "changed",
			    gpa_option_changed_combo_entry, cdi);

	path = gpa_options_dup_path (options);
	g_hash_table_insert (cdi->widgets, path, combo);

	return combo;
}


static GtkWidget*
gpa_create_combo_from_options (GpaConfigDialogInfo *cdi,
			       GpaOptions *options,
			       GtkWidget *table)
{
	GtkWidget *combo;
	GtkWidget *label;
	gchar *label_text;
	gint row;

	debug (FALSE, "");
	
	g_return_val_if_fail (GPA_IS_OPTIONS (options), NULL);
	g_return_val_if_fail (GPA_IS_SETTINGS (cdi->loaded_settings), NULL);

	combo = gpa_create_combo_only (cdi, options);

	label_text = g_strdup_printf ("%s :", gpa_options_get_name (options));
	label = gtk_label_new (label_text);
	gtk_misc_set_alignment (GTK_MISC (label), 0.0, 0.5);
	g_free (label_text);

	if (table != NULL) {
		row = GTK_TABLE (table)->nrows;
		gtk_table_attach (GTK_TABLE (table), label,
				  0, 1, row, row+1,
				  GTK_FILL|GTK_EXPAND, 0, GPA_CONFIG_TABLE_PAD, GPA_CONFIG_TABLE_PAD);
		gtk_table_attach (GTK_TABLE (table), combo,
				  1, 2, row, row+1,
				  GTK_FILL|GTK_EXPAND, 0, GPA_CONFIG_TABLE_PAD, GPA_CONFIG_TABLE_PAD);
	}
	
	return combo;
}

static GtkWidget*
gpa_create_combo_from_options_no_label (GpaConfigDialogInfo *cdi,
					GpaOptions *options,
					GtkWidget *table)
{
	GtkWidget *combo;
	gint row;

	debug (FALSE, "");
	
	g_return_val_if_fail (GPA_IS_OPTIONS (options), NULL);
	g_return_val_if_fail (GPA_IS_SETTINGS (cdi->loaded_settings), NULL);

	combo = gpa_create_combo_only (cdi, options);

	if (table != NULL) {
		row = GTK_TABLE (table)->nrows;
		gtk_table_attach (GTK_TABLE (table), combo,
				  1, 2, row, row+1,
				  GTK_FILL|GTK_EXPAND, 0, GPA_CONFIG_TABLE_PAD, GPA_CONFIG_TABLE_PAD);
	}
	
	return combo;
}

static GtkWidget*
gpa_create_radio_button_from_option (GpaConfigDialogInfo *cdi,
				     GpaOption *option,
				     GSList **group_incoming)
{
	GtkWidget *radio_button;
	GSList *group;

	debug (FALSE, "");

	group = *group_incoming;
	
	radio_button = gtk_radio_button_new_with_label (group, gpa_option_get_name (option));
	group = gtk_radio_button_group (GTK_RADIO_BUTTON (radio_button));

	gtk_object_set_data (GTK_OBJECT (radio_button), "option", option);
	
	gtk_signal_connect (GTK_OBJECT (radio_button), "pressed",
			    GTK_SIGNAL_FUNC (gpa_option_changed),
			    cdi);
	
	*group_incoming = group;
	
	return radio_button;
}


GtkWidget*
gpa_create_radio_buttons_from_options (GpaConfigDialogInfo *cdi,
				       const GpaOptions *options,
				       gboolean homogeneous)
{
	GpaOption *option;
	GtkWidget *radio_button = NULL;
	GtkWidget *vbox;
	GSList *group = NULL;
	GList *list;
	gchar *path;
	gint index = 0;
	gint selected = 0;

	debug (FALSE, "");

	g_return_val_if_fail (GPA_IS_OPTIONS (options), NULL);
	g_return_val_if_fail (GPA_IS_SETTINGS (cdi->loaded_settings), NULL);

	vbox  = gtk_vbox_new (homogeneous, 0);

	list = gpa_options_get_children (options);
	for ( ;list != NULL; list = list->next) {
		option = (GpaOption*) list->data;
		radio_button = gpa_create_radio_button_from_option (cdi,
								    option,
								    &group);
		gtk_box_pack_start (GTK_BOX (vbox), radio_button, FALSE, TRUE, 0);
		if (gpa_option_is_selected (cdi->loaded_settings, option))
			selected = index;
		index++;
	}

        /* Select the first resolution */
	gpa_gtk_radio_button_select (GTK_RADIO_BUTTON(radio_button)->group, selected);

	/* Only add the toplevel options to the hash */
	if (gpa_options_get_parent (options) == NULL) {
		path = gpa_options_dup_path (options);
		g_hash_table_insert (cdi->widgets, path, radio_button);
	}

	debug (FALSE, "end");
	
	return vbox;
}

static GtkWidget *
gpa_create_widget_from_children_options (GpaConfigDialogInfo *cdi,
					 const GpaOptions *options)
{
	GtkWidget *vbox = NULL;

	debug (FALSE, "");

	if (!gpa_options_have_children (options))
		return NULL;

	/* FIXME */
	vbox = gtk_vbox_new (FALSE, 0);

	return vbox;
}

static void
gpa_set_default_frame (GpaConfigDialogInfo *cdi,
		       GtkWidget *frame)
{
	GpaOptions *options;
	GpaOption *option;
	GpaOption *selected_option = NULL;
	GtkWidget *vbox;
	GList *list;

	debug (FALSE, "");
	
	g_return_if_fail (GPA_IS_SETTINGS (cdi->loaded_settings));
	g_return_if_fail (GTK_IS_VBOX (frame));
	
	options = gpa_model_get_options_from_id (gpa_settings_get_model (cdi->loaded_settings),
						 "Resolution");
	list = gpa_options_get_children (options);
	
	for (; list != NULL; list = list->next) {
		option = (GpaOption*) list->data;
		if (gpa_option_is_selected (cdi->loaded_settings, option))
			selected_option = option;
	}

	g_return_if_fail (selected_option != NULL);

	if (gpa_option_get_children (selected_option) != NULL) {
		vbox = gpa_create_radio_buttons_from_options (cdi,
							      gpa_option_get_children (selected_option),
							      FALSE);
		if (vbox)
			gtk_box_pack_start (GTK_BOX (frame), vbox, FALSE, FALSE, 0);
	}

	gtk_widget_show_all (frame);
}

/**
 * gpa_create_hbox_from_options:
 * @printer: 
 * @options_name: 
 * 
 * This will create a main and a children radio group
 * 
 * Return Value: 
 **/
static GtkWidget*
gpa_create_radio_buttons_from_options_w_children (GpaConfigDialogInfo *cdi,
						  const GpaOptions *options)
{
	GtkWidget *main_vbox;
	GtkWidget *children_vbox;
	GtkWidget *hbox;

	debug (FALSE, "");

	g_return_val_if_fail (GPA_IS_SETTINGS (cdi->loaded_settings), NULL);
	g_return_val_if_fail (GPA_IS_OPTIONS (options), NULL);
	
	hbox  = gtk_hbox_new (TRUE, 0);

	children_vbox = gpa_create_widget_from_children_options (cdi, options);
	main_vbox     = gpa_create_radio_buttons_from_options (cdi, options, TRUE);
	if (main_vbox == NULL)
		return FALSE;

	gtk_box_pack_start (GTK_BOX (hbox), main_vbox,  FALSE, TRUE, 10);
	if (children_vbox) {
		gtk_object_set_data (GTK_OBJECT (main_vbox),
				     "children_vbox", children_vbox);
		gtk_box_pack_start (GTK_BOX (hbox), children_vbox, FALSE, TRUE, 10);
	}

	if (children_vbox)
		gpa_set_default_frame (cdi, children_vbox);

	debug (FALSE, "NULL");
	
	return hbox;
}

static GtkWidget *
gpa_create_check_button_from_options (GpaConfigDialogInfo *cdi,
				      GpaOptions *options)
{
	GtkWidget *button;
	GpaOption *option;
	gchar *path;
	gboolean active;
	

	g_return_val_if_fail (GPA_IS_SETTINGS (cdi->loaded_settings), NULL);
	g_return_val_if_fail (GPA_IS_OPTIONS (options), NULL);
	g_return_val_if_fail (gpa_options_get_type (options) == GPA_OPTIONS_TYPE_BOOLEAN, NULL);

	button = gtk_check_button_new_with_label (gpa_options_get_name (options));

	path = gpa_options_dup_path (options);
	g_hash_table_insert (cdi->widgets, path, button);

	option = gpa_options_get_selected_option (cdi->loaded_settings,
						  options,
						  FALSE);
	if (strcmp (GPA_TAG_TRUE, gpa_option_get_id (option))==0)
		active = TRUE;
	else
		active = FALSE;

	gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (button), active);
	
	gtk_object_set_data (GTK_OBJECT (button), "options", options);
	gtk_signal_connect (GTK_OBJECT (button), "toggled",
			    GTK_SIGNAL_FUNC (gpa_option_changed_boolean),
			    cdi);
	
	return button;
}

static GtkWidget *
gpa_create_widget_from_options_boolean (GpaConfigDialogInfo *cdi,
					GpaOptions *options,
					GtkWidget *table)
{
	GtkWidget *check_button;

	debug (FALSE, "");

	g_return_val_if_fail (GPA_IS_OPTIONS (options), NULL);
	g_return_val_if_fail (GPA_IS_SETTINGS (cdi->loaded_settings), NULL);

	check_button = gpa_create_check_button_from_options (cdi, options);
	if (check_button == NULL)
		return NULL;

	if (table != NULL) {
		gint row = GTK_TABLE (table)->nrows;
		gtk_table_attach (GTK_TABLE (table), check_button,
				  0, 1, row, row+1,
				  GTK_FILL|GTK_EXPAND, 0, GPA_CONFIG_TABLE_PAD, GPA_CONFIG_TABLE_PAD);
#if 0	
		gtk_table_set_row_spacing (GTK_TABLE (table), row-1, );
#endif	
	}

	return check_button;
}

static GtkWidget *
gpa_create_widget_from_options_numeric (GpaConfigDialogInfo *cdi,
					GpaOptions *options,
					GtkWidget *table)
{
	GpaOption *option;
	GList *children;

	GtkObject *adjustment;
	GtkWidget *hbox = NULL;
	GtkWidget *spin;
	GtkWidget *name_label;
	GtkWidget *unit_label = NULL;

	gchar *unit;
	gchar *name;
	gchar *path;

	gint value = 1;
	gint lower = 0;
	gint upper = 999999;

	gint step_increment = 1;
	gint page_increment = 265;
	gint page_size = 1;

	debug (FALSE, "");

	g_return_val_if_fail (GPA_IS_OPTIONS (options),   NULL);
	g_return_val_if_fail (GPA_IS_SETTINGS (cdi->loaded_settings), NULL);
	g_return_val_if_fail (g_list_length (gpa_options_get_children (options)) == 1, NULL);

	children  = gpa_options_get_children (options);

	g_return_val_if_fail (children != NULL, NULL);

	option = (GpaOption *) children->data;

	/* Get the values needed */
	unit = gpa_option_value_dup (option, GPA_TAG_UNIT);
	gpa_settings_value_get_from_option_int (cdi->loaded_settings, option, &value);
	gpa_option_value_get_int (option, GPA_TAG_RANGE_MIN, &lower);
	gpa_option_value_get_int (option, GPA_TAG_RANGE_MAX, &upper);

	adjustment = gtk_adjustment_new (value,
					 lower,
					 upper,
					 step_increment,
					 page_increment,
					 page_size);
	
	spin  = gtk_spin_button_new (GTK_ADJUSTMENT(adjustment), 10, 0);
	gtk_widget_set_usize (spin, 80, 0);

	gtk_object_set_data (GTK_OBJECT (spin), "option", option);
	gtk_signal_connect (GTK_OBJECT (spin), "changed",
			    GTK_SIGNAL_FUNC (gpa_option_changed_numeric),
			    cdi);
	
	gtk_widget_show (spin);

	name = g_strdup_printf ("%s : ", gpa_option_get_name (option));
	name_label = gtk_label_new (name);
	if (unit != NULL)
		unit_label = gtk_label_new (unit);

	g_free (name);
	if (unit != NULL)
		g_free (unit);

	path = gpa_options_dup_path (options);
	g_hash_table_insert (cdi->widgets, path, spin);

	hbox  = gtk_hbox_new (FALSE, 0);
	gtk_box_pack_start (GTK_BOX (hbox), name_label, FALSE, TRUE, 0);
	gtk_box_pack_start (GTK_BOX (hbox), spin, FALSE, TRUE, 0);
	if (unit != NULL)
		gtk_box_pack_start (GTK_BOX (hbox), unit_label, FALSE, TRUE, 0);
	
	if (table != NULL) {
		gint row = GTK_TABLE (table)->nrows;
		gtk_table_attach (GTK_TABLE (table), hbox,
				  0, 3, row, row+1,
				  GTK_FILL|GTK_EXPAND, 0, GPA_CONFIG_TABLE_PAD, GPA_CONFIG_TABLE_PAD);
#if 0	
		gtk_table_set_row_spacing (GTK_TABLE (table), row-1, );
#endif
	}

	return hbox;
}

static GtkWidget *
gpa_create_widget_from_options_double (GpaConfigDialogInfo *cdi,
				       GpaOptions *options,
				       GtkWidget *table)
{
	GpaOption *option;
	GList *children;
	GtkObject *adjustment;
	GtkWidget *slider;
	GtkEntry *entry;

	double value = 1;
	double lower = 0;
	double upper = 999999;

	gchar *path;

	gint step_increment = 1;
	gint page_increment = 265;
	gint page_size = 1;

	debug (FALSE, "");

	g_return_val_if_fail (GPA_IS_OPTIONS (options), NULL);
	g_return_val_if_fail (GPA_IS_SETTINGS (cdi->loaded_settings), NULL);
	g_return_val_if_fail (g_list_length (gpa_options_get_children (options)) == 1, NULL);

	children = gpa_options_get_children (options);

	option = (GpaOption *) children->data;

	/* Get the values needed */
	gpa_settings_value_get_from_option_double (cdi->loaded_settings, option, &value);
	gpa_option_value_get_double (option, GPA_TAG_RANGE_MIN, &lower);
	gpa_option_value_get_double (option, GPA_TAG_RANGE_MAX, &upper);


	adjustment = gtk_adjustment_new (value,
					 lower,
					 upper + .999,
					 step_increment,
					 page_increment,
					 page_size);
	
	slider = gpa_slider (cdi, option, GTK_ADJUSTMENT (adjustment), &entry);

	g_return_val_if_fail (GTK_IS_ENTRY (entry), NULL);

	gtk_widget_show (slider);

	path = gpa_options_dup_path (options);
	g_hash_table_insert (cdi->widgets, path, entry);

	if (table != NULL) {
		gint row = GTK_TABLE (table)->nrows;
		gtk_table_attach (GTK_TABLE (table), slider,
				  0, 3, row, row+1,
				  GTK_FILL|GTK_EXPAND, 0, GPA_CONFIG_TABLE_PAD, GPA_CONFIG_TABLE_PAD);
#if 0	
		gtk_table_set_row_spacing (GTK_TABLE (table), row-1, );
#endif	
	}

	return slider;
}

static GtkWidget *
gpa_create_widget_from_options_pickone (GpaConfigDialogInfo *cdi,
					GpaOptions *options,
					GtkWidget *table)
{
	GpaPickoneType pickone_type;
	GtkWidget *vbox;
	gint row;
	
	debug (FALSE, "");

	g_return_val_if_fail (GPA_IS_OPTIONS (options), NULL);
	g_return_val_if_fail (GPA_IS_SETTINGS (cdi->loaded_settings), NULL);


	pickone_type = gpa_options_get_pickone_type (options);

	switch (pickone_type) {
	case GPA_PICKONE_RADIO:
		vbox = gpa_create_radio_buttons_from_options_w_children (cdi, options);
		g_return_val_if_fail (GTK_IS_WIDGET (vbox), NULL);
		row = GTK_TABLE (table)->nrows;
		gtk_table_attach (GTK_TABLE (table), vbox,
				  0, 3, row, row+1,
				  GTK_FILL|GTK_EXPAND, 0, GPA_CONFIG_TABLE_PAD, GPA_CONFIG_TABLE_PAD);
		return vbox;
		break;
	case GPA_PICKONE_COMBO:
		return gpa_create_combo_from_options (cdi, options, table);
		break;
	default:
		gpa_error ("Pickone type not implemented (%i)\n", pickone_type);
	}
#if 0	
	return  gpa_create_option_menu_from_options (cdi, options, table);
#endif
	return NULL;
}

/**
 * gpa_create_widget_from_options:
 * @cdi: 
 * @options: 
 * @table: 
 * @false: 
 * @: 
 * 
 * Table can be NULL ... 
 * 
 * Return Value: 
 **/
static GtkWidget *
gpa_create_widget_from_options (GpaConfigDialogInfo *cdi,
				GpaOptions *options,
				GtkWidget *table)
{
	GtkWidget *frame = NULL;

	debug (FALSE, "");

	g_return_val_if_fail (GPA_IS_OPTIONS (options), NULL);
	g_return_val_if_fail (GPA_IS_SETTINGS (cdi->loaded_settings), NULL);

	switch (gpa_options_get_type (options)) {
	case GPA_OPTIONS_TYPE_PICKONE:
		frame = gpa_create_widget_from_options_pickone (cdi, options, table);
		break;
	case GPA_OPTIONS_TYPE_PICKMANY:
		gpa_error ("Pickmany not implemented\n");
		break;
	case GPA_OPTIONS_TYPE_BOOLEAN:
		frame = gpa_create_widget_from_options_boolean (cdi, options, table);
		break;
	case GPA_OPTIONS_TYPE_NUMERIC:
		frame = gpa_create_widget_from_options_numeric (cdi, options, table);
		break;
	case GPA_OPTIONS_TYPE_DOUBLE:
		frame = gpa_create_widget_from_options_double (cdi, options, table);
		break;
	case GPA_OPTIONS_TYPE_ERROR:
		gpa_error ("Error. Type is : GPA_OPTIONS_TYPE_ERROR\n");
		break;
	default:
		g_warning ("Unknown option type not implemented");
	}

	debug (FALSE, "end");
	
	return frame;
}
	
static GtkWidget *
gpa_create_frame_from_name (GpaConfigDialogInfo *cdi,
			    const gchar *options_name,
			    gboolean required)
{
	GpaOptions *options;
	GtkWidget *frame;
	GtkWidget *table;
	GtkWidget *vbox;

	debug (FALSE, "");

	g_return_val_if_fail (GPA_IS_SETTINGS (cdi->loaded_settings), NULL);
	g_return_val_if_fail (options_name != NULL, NULL);

	options = gpa_model_get_options_from_id (gpa_settings_get_model (cdi->loaded_settings),
						 options_name);

	if (options == NULL) {
		if (required)
			gpa_error ("Could not create the required frame \"%s\"\n",
				   options_name);
		return NULL;
	}

	vbox = gtk_vbox_new (FALSE, 0);
	gtk_container_set_border_width (GTK_CONTAINER (vbox), 0);
	
	table = gtk_table_new (0, 0, FALSE);
	gtk_table_set_row_spacings (GTK_TABLE (table), GPA_CONFIG_TABLE_ROW_SPACING);
	gtk_table_set_row_spacings (GTK_TABLE (table), GPA_CONFIG_TABLE_COL_SPACING);
	gtk_container_set_border_width (GTK_CONTAINER (table), GPA_CONFIG_TABLE_BORDER);
	
	frame = gtk_frame_new (gpa_options_get_frame (options));
	gtk_container_add (GTK_CONTAINER (frame), table);
	
	gpa_create_widget_from_options (cdi, options, table);
	gtk_box_pack_start (GTK_BOX (vbox), frame, FALSE, FALSE, 0);

	return vbox;
}
	

static GtkWidget *
gpa_create_frame_from_name_no_label (GpaConfigDialogInfo *cdi,
				     const gchar *options_name,
				     gboolean required)
{
	GpaOptions *options;
	GtkWidget *frame;
	GtkWidget *table;
	GtkWidget *vbox;

	debug (FALSE, "");

	g_return_val_if_fail (GPA_IS_SETTINGS (cdi->loaded_settings), NULL);
	g_return_val_if_fail (options_name != NULL, NULL);

	options = gpa_model_get_options_from_id (gpa_settings_get_model (cdi->loaded_settings),
						 options_name);

	if (options == NULL) {
		if (required)
			gpa_error ("Could not create the required frame \"%s\"\n",
				   options_name);
		return NULL;
	}

	vbox = gtk_vbox_new (FALSE, 0);
	gtk_container_set_border_width (GTK_CONTAINER (vbox), 0);
	
	table = gtk_table_new (0, 0, FALSE);
	gtk_table_set_row_spacings (GTK_TABLE (table), GPA_CONFIG_TABLE_ROW_SPACING);
	gtk_table_set_col_spacings (GTK_TABLE (table), GPA_CONFIG_TABLE_COL_SPACING);
	gtk_container_set_border_width (GTK_CONTAINER (table), GPA_CONFIG_TABLE_BORDER);
	
	frame = gtk_frame_new (gpa_options_get_frame (options));
	gtk_container_add (GTK_CONTAINER (frame), table);
	
	gpa_create_combo_from_options_no_label (cdi, options, table);
	gtk_box_pack_start (GTK_BOX (vbox), frame, FALSE, FALSE, 0);

	return vbox;
}

static GtkWidget *
gpa_create_vbox_from_generic_options (GpaConfigDialogInfo *cdi,
				      GpaOptionsGroup group)
{
	const GpaModel *model;
	const gchar *frame_name;
	GHashTable *frames;
 	GpaOptions *options;
	GtkWidget *vbox;
	GtkWidget *nth_widget;
	GtkWidget *frame_table;
	GtkWidget *frame;
	gboolean empty;
	GList *list;

	debug (FALSE, "");

	g_return_val_if_fail (GPA_IS_SETTINGS (cdi->loaded_settings), NULL);

	vbox = gtk_vbox_new (FALSE, GPA_CONFIG_PAGE_VBOX_SPACING);
	gtk_container_set_border_width (GTK_CONTAINER (vbox), 0);

	frames = g_hash_table_new (g_str_hash, g_str_equal);
		
	empty = TRUE;

	model = gpa_settings_get_model (cdi->loaded_settings);
	list = gpa_model_get_options_list (model);
	
	for (; list != NULL; list = list->next) {
		options = (GpaOptions*) list->data;

		g_return_val_if_fail (options != NULL, NULL);
		
		if (gpa_options_get_group (options) != group)
			continue;
		if (gpa_options_get_content (options) != GPA_CONTENT_GENERIC)
			continue;
		
		frame_name = gpa_options_get_frame (options);
		frame_table = g_hash_table_lookup (frames, frame_name);
		if (frame_table == NULL) {
			frame_table = gtk_table_new (0, 0, FALSE);
			gtk_table_set_row_spacings (GTK_TABLE (frame_table),
						    GPA_CONFIG_TABLE_ROW_SPACING);
			gtk_table_set_row_spacings (GTK_TABLE (frame_table),
						    GPA_CONFIG_TABLE_COL_SPACING);
			gtk_container_set_border_width (GTK_CONTAINER (frame_table),
							GPA_CONFIG_TABLE_BORDER);
			
			g_hash_table_insert (frames, (gchar *)frame_name, frame_table);
			frame = gtk_frame_new (frame_name);
			gtk_container_add (GTK_CONTAINER (frame), frame_table);
			gtk_box_pack_start (GTK_BOX (vbox), frame, FALSE, TRUE, 0);
		}

		nth_widget = gpa_create_widget_from_options (cdi, options, frame_table);
		if (nth_widget == NULL)
			return NULL;

		empty = FALSE;
	}

	if (empty) {
		gtk_object_unref (GTK_OBJECT (vbox));
		return NULL;
	}

	debug (FALSE, "end");
	
	return vbox;
}

static GtkWidget*
gpa_create_paper_orientation_table (GpaConfigDialogInfo *cdi)
{
	GtkWidget *table;
	GtkWidget *portrait;
	GtkWidget *landscape;
	GtkWidget *rotate;
	GSList    *group = NULL;
	gchar *orientation_str;

	debug (FALSE, "");

	g_return_val_if_fail (GPA_IS_SETTINGS (cdi->loaded_settings), NULL);
	
	table = gtk_table_new (2, 2, FALSE);
	gtk_table_set_row_spacings (GTK_TABLE (table), GPA_CONFIG_TABLE_ROW_SPACING);
	gtk_table_set_row_spacings (GTK_TABLE (table), GPA_CONFIG_TABLE_COL_SPACING);
	gtk_container_set_border_width (GTK_CONTAINER (table), GPA_CONFIG_TABLE_BORDER);

	portrait  = gtk_radio_button_new_with_label (group, _("Portrait"));
	group     = gtk_radio_button_group (GTK_RADIO_BUTTON(portrait));
	landscape = gtk_radio_button_new_with_label (group, _("Landscape"));
	group     = gtk_radio_button_group (GTK_RADIO_BUTTON(portrait));
	rotate    = gtk_check_button_new_with_label (_("Rotate 180"));

	gtk_table_attach_defaults (GTK_TABLE(table), portrait, 0, 1, 0, 1);
	gtk_table_attach_defaults (GTK_TABLE(table), landscape, 0, 1, 1, 2);
	gtk_table_attach_defaults (GTK_TABLE(table), rotate, 1, 2, 0, 1);

#if 0
	gtk_table_set_row_spacing (GTK_TABLE (table), 0, GPA_CONFIG_PAGE_VBOX_ROW_SPACING);
#endif

	orientation_str = gpa_settings_value_dup (cdi->loaded_settings, GPA_TAG_ORIENTATION);

	if ( (orientation_str == NULL) ||
	     (strcmp (orientation_str, GPA_TAG_PORTRAIT) == 0) ||
	     (strcmp (orientation_str, GPA_TAG_LANDSCAPE) == 0))
		gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (rotate), FALSE);
	else if	( (strcmp (orientation_str, GPA_TAG_REVERSE_PORTRAIT) == 0) ||
		  (strcmp (orientation_str, GPA_TAG_REVERSE_LANDSCAPE) == 0) )
		gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (rotate), TRUE);
	else {
		gpa_error ("Unrecognized orientation %s\n", orientation_str);
		return NULL;
	}
	
	if ( (orientation_str == NULL) ||
	     (strcmp (orientation_str, GPA_TAG_PORTRAIT) == 0) ||
	     (strcmp (orientation_str, GPA_TAG_REVERSE_PORTRAIT) == 0))
		gpa_gtk_radio_button_select (group, 0);
	else if ( (strcmp (orientation_str, GPA_TAG_LANDSCAPE) == 0) ||
		  (strcmp (orientation_str, GPA_TAG_REVERSE_LANDSCAPE) == 0))
		gpa_gtk_radio_button_select (group, 1);
	else {
		gpa_error ("Unrecognized orientation %s\n", orientation_str);
		return NULL;
	}

	/* If we don't have a value, insert it */
	if (orientation_str == NULL) {
		gpa_settings_value_insert (cdi->loaded_settings,
					   GPA_TAG_ORIENTATION,
					   GPA_TAG_PORTRAIT);
	}
	
	/* Attach the signals */
	gtk_signal_connect (GTK_OBJECT (rotate), "toggled",
			    GTK_SIGNAL_FUNC (gpa_rotate_toggled),
			    cdi);
	gtk_signal_connect (GTK_OBJECT (portrait), "toggled",
			    GTK_SIGNAL_FUNC (gpa_orientation_toggled),
			    cdi);

	if (orientation_str != NULL)
		g_free (orientation_str);
	
	debug (FALSE, "end");

	return table;
}

static GtkWidget*
gpa_create_paper_orientation_frame (GpaConfigDialogInfo *cdi)
{
	GtkWidget *frame;
	GtkWidget *table;

	debug (FALSE, "");

	g_return_val_if_fail (GPA_IS_SETTINGS (cdi->loaded_settings), NULL);
	
	frame = gtk_frame_new (_("Orientation"));

	table = gpa_create_paper_orientation_table (cdi);

	g_return_val_if_fail (table != NULL, NULL);
	
	/* add to the vbox */
	gtk_container_add (GTK_CONTAINER (frame), table);

	debug (FALSE, "end");
	
	return frame;
}

gboolean
gpa_load_generic_page (GpaConfigDialogInfo *cdi, GpaOptionsGroup group, const gchar *page_name)
{
	GtkWidget *vbox;
	GtkWidget *lower_vbox;
	GtkWidget *label;

	debug (FALSE, "");


	/* Now load generic options  */
	lower_vbox = gpa_create_vbox_from_generic_options (cdi, group);
	if (lower_vbox == NULL)
		return TRUE;

	vbox = gtk_vbox_new (FALSE, GPA_CONFIG_PAGE_VBOX_SPACING);
	gtk_container_set_border_width (GTK_CONTAINER (vbox), GPA_CONFIG_PAGE_VBOX_BORDER);
	gtk_box_pack_start (GTK_BOX (vbox), lower_vbox, FALSE, TRUE, 0);

	/* Insert the page into the notebook */
	label = gtk_label_new (page_name);
	gtk_notebook_insert_page (GTK_NOTEBOOK (cdi->notebook), vbox, label, cdi->page++);

	debug (FALSE, "end");

	return TRUE;
}

gboolean
gpa_load_paper_page (GpaConfigDialogInfo *cdi)
{
	GtkWidget *hbox_top;
	GtkWidget *vbox_top;
	GtkWidget *vbox; 
	GtkWidget *label;
	
	GtkWidget *sizes_frame;
	GtkWidget *orient_frame;
	GtkWidget *media_frame;
	GtkWidget *source_frame;
	GtkWidget *generic_vbox;

	debug (FALSE, "");

	vbox = gtk_vbox_new (FALSE, GPA_CONFIG_PAGE_VBOX_SPACING);
	gtk_container_set_border_width (GTK_CONTAINER (vbox), GPA_CONFIG_PAGE_VBOX_BORDER);
	
	vbox_top  = gtk_vbox_new (FALSE, GPA_CONFIG_PAGE_VBOX_SPACING);
	hbox_top  = gtk_hbox_new (FALSE, GPA_CONFIG_PAGE_VBOX_SPACING);

	sizes_frame  = gpa_create_frame_from_name_no_label (cdi, GPA_TAG_MEDIA_SIZE, TRUE);
	orient_frame = gpa_create_paper_orientation_frame (cdi);
	if (orient_frame == NULL)
		return FALSE;
	if (sizes_frame == NULL)
		return FALSE;
	
	media_frame  = gpa_create_frame_from_name (cdi, GPA_TAG_MEDIA_TYPE, FALSE);
	source_frame = gpa_create_frame_from_name (cdi, GPA_TAG_INPUT_TRAY, FALSE);

	/* Now load generic options  */
	generic_vbox = gpa_create_vbox_from_generic_options (cdi, GPA_GROUP_PAPER);
	
	label = gtk_label_new ("Show an image of the paper here");

	/* Pack start everything */
	gtk_box_pack_start (GTK_BOX (vbox_top), sizes_frame,  FALSE, TRUE, 0);
	gtk_box_pack_start (GTK_BOX (vbox_top), orient_frame, FALSE, TRUE, 0);
        gtk_box_pack_start (GTK_BOX (hbox_top), vbox_top,     FALSE, TRUE, 0);
        gtk_box_pack_start (GTK_BOX (hbox_top), label,        FALSE, TRUE, 0);
	gtk_box_pack_start (GTK_BOX (vbox), hbox_top,         FALSE, TRUE, 0);
	if (media_frame != NULL)
		gtk_box_pack_start (GTK_BOX (vbox), media_frame, FALSE, TRUE, 0);
	if (source_frame != NULL)
		gtk_box_pack_start (GTK_BOX (vbox), source_frame, FALSE, TRUE, 0);
	if (generic_vbox != NULL)
		gtk_box_pack_start (GTK_BOX (vbox), generic_vbox, FALSE, TRUE, 0);
	

	/* Insert the page into the notebook */
	label = gtk_label_new (_("Paper Settings"));
	gtk_notebook_insert_page (GTK_NOTEBOOK (cdi->notebook), vbox, label,  cdi->page++);

	debug (FALSE, "end");

	return TRUE;
}

#if 0 /* NOt used */
static GtkWidget*
gpa_create_menu_item_from_backend (GpaBackend *backend,
				   GpaSettings *settings,
				   GHashTable *widgets_hash)
{
	GtkWidget *menu_item;

	debug (FALSE, "");
	
	menu_item = gtk_menu_item_new_with_label (gpa_backend_get_id (backend));

	gtk_object_set_data (GTK_OBJECT (menu_item), "backend", backend);
	gtk_object_set_data (GTK_OBJECT (menu_item), "settings", settings);

	gtk_signal_connect (GTK_OBJECT (menu_item), "activate",
			    GTK_SIGNAL_FUNC (gpa_backend_changed),
			    backend);

	debug (FALSE, "false");
	
	return menu_item;
}

static gboolean
gpa_config_backend_option_menu_attach (GpaPrinter *printer,
				       GtkWidget *table,
				       GHashTable *widgets_hash)
{
	GpaSettings *settings;
	GpaBackend *backend;
	GtkWidget *backend_label;
	GtkWidget *option_menu;
	GtkWidget *menu_item;
	GtkWidget *menu;
	GList *list;
	gchar *backend_str;
#if 0	
	gchar *path;
#endif
	gint index = 0;
	gint selected = 0;

	/* DISABLED by Chema */
	return TRUE;
		
	g_return_val_if_fail (GPA_IS_PRINTER (printer), FALSE);
	g_return_val_if_fail (GTK_IS_TABLE (table), FALSE);

	settings = gpa_printer_settings_get_selected (printer);

	g_return_val_if_fail (GPA_IS_SETTINGS (settings), FALSE);

	if (gpa_printer_settings_list_get_size (printer) < 1)
		return FALSE;

	backend_label = gtk_label_new ("Backend");
	gtk_table_attach (GTK_TABLE (table), backend_label,
			  0, 1, 5, 6,
			  0, 0, 0, 0 );
		

	backend_str = gpa_settings_value_dup_required (settings, GPA_TAG_BACKEND);
	if (backend_str == NULL)
		return FALSE;
		    
	list = gpa_printer_get_backend_list (printer);
		
	menu = gtk_menu_new ();
	for (; list != NULL; list = list->next) {
		backend = (GpaBackend *) list->data;

		g_return_val_if_fail (gpa_backend_get_id (backend) != NULL, FALSE);
		
		menu_item = gpa_create_menu_item_from_backend (backend,
							       settings,
							       widgets_hash);

		gtk_widget_show (menu_item);
		gtk_menu_append (GTK_MENU (menu), menu_item);
		if (strcmp (gpa_backend_get_id (backend), backend_str) == 0)
			selected = index;
		index++;
	}

	g_free (backend_str);
	option_menu = gtk_option_menu_new ();
	gtk_option_menu_set_menu    (GTK_OPTION_MENU (option_menu), menu);
	gtk_option_menu_set_history (GTK_OPTION_MENU (option_menu), selected);

#if 0	
	path = g_strdup_printf ("%s%s", GPA_TAG_VALUE_PATH_PREFIX, GPA_TAG_BACKEND);
	g_hash_table_insert (widgets_hash, path, option_menu);
#else
	g_print ("Not inserting Backend\n");
#endif	
	gtk_table_attach (GTK_TABLE (table), option_menu,
			  1, 2, 5, 6,
			  0, 0, 0, 0 );

	return TRUE;
}
#endif /* Disabled for now */

