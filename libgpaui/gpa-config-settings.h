#ifndef __GNOME_PRINT_ADMIN_CONFIG_SETTINGS_H__
#define __GNOME_PRINT_ADMIN_CONFIG_SETTINGS_H__

BEGIN_GNOME_DECLS

extern gboolean   debug_turned_on;

gboolean gpa_load_general_page_settings_stuff (GpaConfigDialogInfo *cdi);

END_GNOME_DECLS

#endif /* __GNOME_PRINT_ADMIN_CONFIG_SETTINGS_H__ */




