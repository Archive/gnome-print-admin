/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Copyright (C) 2000 Jose M Celorio
 *
 * gpa-config-settings.c : Handles the settting loading/copying/rename/deleting
 *                         of the printer config dialog.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Authors :
 *  Chema Celorio <chema@celorio.com>
 *
 */

#include "gpa-defs.h"

#include <glib.h>
#include <gtk/gtkentry.h>
#include <gtk/gtkclist.h>
#include <gtk/gtkvbox.h>
#include <gtk/gtkbox.h>
#include <gtk/gtkbbox.h>
#include <libgnomeui/gnome-dialog.h>
#include <libgnomeui/gnome-stock.h>

#include "gpa-config.h"
#include "gpa-config-settings.h"
#include "gpa-items-update.h"

#include <libgpa/gpa-settings.h>

/* This should go in gpa-utils.c */
static void
gpa_dialog_destroy (GtkWidget *dialog, gpointer data)
{
	debug (FALSE, "");
	g_return_if_fail (GNOME_IS_DIALOG (dialog));
	gtk_widget_destroy (dialog);
}

static void
gpa_dialog_hide (GtkWidget *dialog, gpointer dummy)
{
	debug (FALSE, "");
	g_return_if_fail (GNOME_IS_DIALOG (dialog));
	gnome_dialog_close (GNOME_DIALOG (dialog));
}

static void
gpa_dialog_hide_by_event (GtkWidget *dialog, gpointer dummy, gpointer dummy2)
{
	debug (FALSE, "");
	gpa_dialog_hide (dialog, NULL);
}
/* --- */


static void
gpa_config_select_clicked (GpaConfigDialogInfo *cdi)
{
	g_return_if_fail (GTK_IS_WIDGET (cdi->dialog));

	if (cdi->selected_settings == NULL) {
		gpa_error ("Why is this button sensitive ?");
		return;
	}

	g_return_if_fail (GPA_IS_SETTINGS (cdi->selected_settings));

	gpa_printer_settings_select (cdi->printer, cdi->selected_settings);
	
	gtk_entry_set_text (GTK_ENTRY (cdi->settings_entry),
			    gpa_settings_get_name (cdi->selected_settings));

	gpa_config_settings_changed (cdi, cdi->selected_settings);
}

static gboolean
gpa_config_rename_dialog_clicked (GtkWidget *dialog,
				  gint button,
				  GpaConfigDialogInfo *cdi)
{
	GpaSettings *settings;
	GpaPrinter *printer;
	GtkCList *clist;
	gchar *name;
	gint row;

	debug (FALSE, "");

	g_return_val_if_fail (GTK_IS_CLIST (cdi->clist), FALSE);

	switch (button) {
	case 1:
		gtk_widget_hide (dialog);
		break;
	case 0:
		settings = cdi->selected_settings;
		printer = cdi->printer;
		clist  = GTK_CLIST (cdi->clist);
		
		row = gtk_clist_find_row_from_data (clist,
						    settings);
		g_return_val_if_fail (row > -1, FALSE);
		name = g_strdup (gtk_entry_get_text (GTK_ENTRY (cdi->rename_entry)));
		if (gpa_settings_name_taken (gpa_printer_settings_list_get (printer), name)) {
			gpa_error ("Name duplicated");
			g_free (name);
			return TRUE;
		}
		if ((name != NULL) && (strlen (name) > 0)) {
			gchar *row_name[2];
			
			gpa_settings_name_replace (settings, name);
			
			row_name[0] = gpa_settings_dup_name (settings);
			row_name[1] = NULL;
			gtk_clist_set_text (clist, row, 0, row_name[0]);

			/* FIXME : We need to update the label this automatically */
			if (gpa_settings_is_selected (settings))
				gtk_entry_set_text (GTK_ENTRY (cdi->settings_entry),
						    gpa_settings_get_name (settings));

			if (row_name [0] != NULL)
				g_free (row_name [0]);
			
			row_name [0] = NULL;
		}
		gtk_widget_hide (dialog);
		break;
	default:
		gpa_error ("action for this button is not yet defined\n");
		/* Unref the settings */
	}

	return TRUE;
}

static GtkWidget*
create_rename_dialog (void)
{
  GtkWidget *rename_dialog;
  GtkWidget *vbox9;
  GtkWidget *vbox10;
  GtkWidget *rename_label;
  GtkWidget *rename_to_entry;
  GtkWidget *hbuttonbox1;
  GtkWidget *button21;
  GtkWidget *button22;

  rename_dialog = gnome_dialog_new (NULL, NULL);
  gtk_object_set_data (GTK_OBJECT (rename_dialog), "rename_dialog", rename_dialog);
  gtk_widget_set_usize (rename_dialog, 240, -2);
  gtk_window_set_modal (GTK_WINDOW (rename_dialog), TRUE);
  gtk_window_set_default_size (GTK_WINDOW (rename_dialog), 300, -1);

  vbox9 = GNOME_DIALOG (rename_dialog)->vbox;
  gtk_object_set_data (GTK_OBJECT (rename_dialog), "vbox9", vbox9);
  gtk_widget_show (vbox9);

  vbox10 = gtk_vbox_new (FALSE, 0);
  gtk_widget_ref (vbox10);
  gtk_object_set_data_full (GTK_OBJECT (rename_dialog), "vbox10", vbox10,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (vbox10);
  gtk_box_pack_start (GTK_BOX (vbox9), vbox10, TRUE, TRUE, 0);

  rename_label = gtk_label_new ("Rename \"Settings Name\" to :");
  gtk_widget_ref (rename_label);
  gtk_object_set_data_full (GTK_OBJECT (rename_dialog), "rename_label", rename_label,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (rename_label);
  gtk_box_pack_start (GTK_BOX (vbox10), rename_label, FALSE, FALSE, 0);
  gtk_misc_set_padding (GTK_MISC (rename_label), 0, 7);

  rename_to_entry = gtk_entry_new ();
  gtk_widget_ref (rename_to_entry);
  gtk_object_set_data_full (GTK_OBJECT (rename_dialog), "rename_to_entry", rename_to_entry,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (rename_to_entry);
  gtk_box_pack_start (GTK_BOX (vbox10), rename_to_entry, FALSE, FALSE, 0);

  hbuttonbox1 = GNOME_DIALOG (rename_dialog)->action_area;
  gtk_object_set_data (GTK_OBJECT (rename_dialog), "hbuttonbox1", hbuttonbox1);
  gtk_widget_show (hbuttonbox1);
  gtk_button_box_set_layout (GTK_BUTTON_BOX (hbuttonbox1), GTK_BUTTONBOX_END);
  gtk_button_box_set_spacing (GTK_BUTTON_BOX (hbuttonbox1), 8);

  gnome_dialog_append_button (GNOME_DIALOG (rename_dialog), GNOME_STOCK_BUTTON_OK);
  button21 = GTK_WIDGET (g_list_last (GNOME_DIALOG (rename_dialog)->buttons)->data);
  gtk_widget_ref (button21);
  gtk_object_set_data_full (GTK_OBJECT (rename_dialog), "button21", button21,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (button21);
  GTK_WIDGET_SET_FLAGS (button21, GTK_CAN_DEFAULT);

  gnome_dialog_append_button (GNOME_DIALOG (rename_dialog), GNOME_STOCK_BUTTON_CANCEL);
  button22 = GTK_WIDGET (g_list_last (GNOME_DIALOG (rename_dialog)->buttons)->data);
  gtk_widget_ref (button22);
  gtk_object_set_data_full (GTK_OBJECT (rename_dialog), "button22", button22,
                            (GtkDestroyNotify) gtk_widget_unref);
  gtk_widget_show (button22);
  GTK_WIDGET_SET_FLAGS (button22, GTK_CAN_DEFAULT);

  return rename_dialog;
}

static gboolean
gpa_config_rename_create_dialog (GpaConfigDialogInfo *cdi)
{
	GtkWidget *rename_dialog;
	GtkWidget *entry;
	GtkWidget *label;

	debug (FALSE, "");

	g_return_val_if_fail (GPA_IS_SETTINGS (cdi->selected_settings), FALSE);

	rename_dialog = create_rename_dialog ();
	entry = GTK_WIDGET (gtk_object_get_data (GTK_OBJECT (rename_dialog), "rename_to_entry"));
	label = GTK_WIDGET (gtk_object_get_data (GTK_OBJECT (rename_dialog), "rename_label"));
	
	if (!GNOME_IS_DIALOG (rename_dialog)) {
		gpa_error ("Could not get the rename dialog from gui (%i)",
			   GPOINTER_TO_INT (rename_dialog));
		return FALSE;
	}
	if (!GTK_IS_ENTRY (entry)) {
		gpa_error ("Could not get the rename entry from gui");
		return FALSE;
	}
	if (!GTK_IS_LABEL (label)) {
		gpa_error ("Could not get the rename entry from gui");
		return FALSE;
	}
	
	gtk_signal_connect (GTK_OBJECT (rename_dialog), "destroy",
			    GTK_SIGNAL_FUNC (gpa_dialog_hide),
			    NULL);
	gtk_signal_connect (GTK_OBJECT (rename_dialog), "delete_event",
			    GTK_SIGNAL_FUNC (gpa_dialog_hide_by_event),
			    NULL);
	gtk_signal_connect (GTK_OBJECT (rename_dialog), "clicked",
			    GTK_SIGNAL_FUNC (gpa_config_rename_dialog_clicked),
			    cdi);

	cdi->rename_dialog = rename_dialog;
	cdi->rename_entry = entry;
	cdi->rename_label = label;
	
	gnome_dialog_close_hides (GNOME_DIALOG (cdi->rename_dialog), TRUE);

	return TRUE;
}

static void
gpa_config_rename_clicked (GpaConfigDialogInfo *cdi)
{
	gchar *label_text;
	
	g_return_if_fail (GNOME_IS_DIALOG (cdi->dialog));

	if (cdi->selected_settings == NULL) {
		gpa_error ("Why is this button sensitive ?");
		return;
	}

	g_return_if_fail (GPA_IS_SETTINGS (cdi->selected_settings));
	
	if (cdi->rename_dialog == NULL)
		gpa_config_rename_create_dialog (cdi);

	/* Set the label */
	label_text = g_strdup_printf (_("Rename \"%s\" to :"),
				      gpa_settings_get_name (cdi->selected_settings));
	gtk_label_set_text (GTK_LABEL (cdi->rename_label), label_text);
	g_free (label_text);

	gnome_dialog_run (GNOME_DIALOG (cdi->rename_dialog));
}


static gboolean
gpa_config_populate_settings_clist_element (GpaSettings *settings,
					    GtkCList *clist)
{
	gchar *name[2];
	gint row;

	g_return_val_if_fail (GPA_IS_SETTINGS (settings), FALSE);
	g_return_val_if_fail (GTK_IS_CLIST (clist), FALSE);

	name [0] = gpa_settings_dup_name (settings);
	name [1] = NULL;

	row = gtk_clist_append (clist, name);
	gtk_clist_set_row_data (clist, row, settings);

	g_free (name[0]);
	
	return TRUE;
}

static void
gpa_config_copy_clicked (GpaConfigDialogInfo *cdi)
{
	const gchar *name;
	GpaSettings *new_settings;
	GpaSettings *selected_settings;
	GList *settings_list;

	g_return_if_fail (GNOME_IS_DIALOG (cdi->dialog));

	if (cdi->selected_settings == NULL) {
		gpa_error ("Why is this button sensitive ?");
		return;
	}

	g_return_if_fail (GPA_IS_SETTINGS (cdi->selected_settings));

	new_settings = gpa_settings_copy (cdi->selected_settings);

	name = gpa_settings_get_free_name (cdi->selected_settings,
					   gpa_printer_settings_list_get (cdi->printer));

	gpa_settings_name_replace (new_settings, name);

	/* Insert the new settings in the printer's settings GList
	 * but make sure we don't change the selected settings
	 */
	settings_list = gpa_printer_settings_list_get (cdi->printer);
	selected_settings = gpa_settings_get_selected (settings_list);
	gpa_printer_settings_append (cdi->printer, new_settings);
	gpa_settings_select (selected_settings, settings_list);

	gpa_config_populate_settings_clist_element (new_settings, GTK_CLIST (cdi->clist));

	gpa_config_set_sensitivity (cdi);
}

static void
gpa_config_delete_clicked (GpaConfigDialogInfo *cdi)
{
	GpaSettings *settings;
	GtkCList *clist;
	
	g_return_if_fail (GNOME_IS_DIALOG (cdi->dialog));

	if (cdi->selected_settings == NULL) {
		gpa_error ("Why is this button sensitive ?");
		return;
	}

	g_return_if_fail (GPA_IS_SETTINGS (cdi->selected_settings));
	
	clist = GTK_CLIST (cdi->clist);
	if (clist->rows < 2) {
		gpa_error ("Why is the delete button activated ?");
		return;
	}

	settings = cdi->selected_settings;
	
	gpa_printer_settings_remove (cdi->printer, settings);
	gtk_clist_remove (clist, GPOINTER_TO_INT (clist->selection->data));

	/* FIXME : THIS IS BROKEN (I THINK) !!!!!!!!!!!!!!! */
	if (gpa_settings_is_selected (settings)) {
		gtk_object_unref (GTK_OBJECT (settings));
		settings = gpa_printer_settings_get_first (cdi->printer);
		gpa_printer_settings_select (cdi->printer, settings);
	}

	settings = gpa_printer_settings_get_selected (cdi->printer);

	gpa_config_settings_changed (cdi, settings);

	gtk_entry_set_text (GTK_ENTRY (cdi->settings_entry),
			    gpa_settings_get_name (settings));

}


static void
gpa_config_dialog_clicked (GtkWidget *button, GpaConfigDialogInfo *cdi)
{
	debug (FALSE, "");

	g_return_if_fail (cdi != NULL);
	g_return_if_fail (GPA_IS_PRINTER (cdi->printer));
	g_return_if_fail (GPA_IS_PRINTER (cdi->real_printer));
	g_return_if_fail (GPA_IS_SETTINGS (gpa_printer_settings_get_first (cdi->printer)));

	if (button == cdi->cancel_button) {
		gpa_dialog_destroy (cdi->dialog, NULL);
	} else if (button == cdi->copy_button) {
		gpa_config_copy_clicked (cdi);
	} else if (button == cdi->rename_button) {
		gpa_config_rename_clicked (cdi);
	} else if (button == cdi->delete_button) {
		gpa_config_delete_clicked (cdi);
	} else if (button == cdi->select_button) {
		gpa_config_select_clicked (cdi);
	} else {
		gpa_error ("action for this button is not yet defined\n");
	}

}




/* ------- Public Funcitions ------- */
static gboolean
gpa_config_populate_settings_clist (GpaConfigDialogInfo *cdi)
{
	GpaSettings *settings;
	GList *list;

	g_return_val_if_fail (cdi != NULL, FALSE);
	g_return_val_if_fail (GTK_IS_CLIST (cdi->clist), FALSE);

	list = gpa_printer_settings_list_get (cdi->printer);
	for (; list != NULL; list = list->next) {
		settings = (GpaSettings *) list->data;
		if (settings == NULL)
			return FALSE;
		if (!gpa_config_populate_settings_clist_element (settings, GTK_CLIST (cdi->clist)))
			return FALSE;
	}

	gpa_config_set_sensitivity (cdi);
	
	return TRUE;
}


void
static gpa_config_set_settings (GtkCList *clist,
				gint row, gint col,
				GdkEvent *dummy,
				GpaConfigDialogInfo *cdi)
{
	g_return_if_fail (cdi != NULL);
	g_return_if_fail (GNOME_IS_DIALOG (cdi->dialog));
	g_return_if_fail (GTK_IS_CLIST (clist));

	if (g_list_length (clist->selection) != 1)
		cdi->selected_settings = NULL;
	else
		cdi->selected_settings = gtk_clist_get_row_data (clist, row);

	gpa_config_set_sensitivity (cdi);
}


static GtkWidget *
gpa_config_load_button (GpaConfigDialogInfo *cdi, const gchar *button_name)
{
	GtkWidget *button;

	button = GTK_WIDGET (gtk_object_get_data (GTK_OBJECT (cdi->dialog), button_name));

	g_return_val_if_fail (GTK_IS_BUTTON (button), NULL);
	
	gtk_signal_connect (GTK_OBJECT (button), "clicked",
			    GTK_SIGNAL_FUNC (gpa_config_dialog_clicked), cdi);

	return button;
}


static void
gpa_config_command_entry_changed (GtkEntry *entry, GpaConfigDialogInfo *cdi)
{
	const gchar *command;
	
	g_return_if_fail (GTK_IS_ENTRY (cdi->command_entry));
	g_return_if_fail (GPA_IS_SETTINGS (cdi->loaded_settings));

	command = gtk_entry_get_text (entry);
	gpa_settings_replace_command (cdi->loaded_settings, command);

}

static void
gpa_config_command_entry_set (GpaConfigDialogInfo *cdi)
{
	g_return_if_fail (GTK_IS_ENTRY (cdi->command_entry));

	gtk_signal_connect (GTK_OBJECT (cdi->command_entry), "changed",
			    GTK_SIGNAL_FUNC (gpa_config_command_entry_changed),
			    cdi);

}

/**
 * gpa_load_general_page_settings_stuff:
 * @cdi: 
 * 
 * Loads all the settings stuff of the main dialog page, it also
 * hooks the buttons needed for modifying settings.
 * 
 * Return Value: 
 **/
gboolean
gpa_load_general_page_settings_stuff (GpaConfigDialogInfo *cdi)
{
	GpaSettings *selected_settings;

	g_return_val_if_fail (cdi != NULL, FALSE);
	g_return_val_if_fail (GPA_IS_PRINTER (cdi->printer), FALSE);

	cdi->clist          = GTK_WIDGET (gtk_object_get_data (GTK_OBJECT (cdi->dialog), "settings_clist"));
	cdi->settings_entry = GTK_WIDGET (gtk_object_get_data (GTK_OBJECT (cdi->dialog), "settings_entry"));
	cdi->command_entry  = GTK_WIDGET (gtk_object_get_data (GTK_OBJECT (cdi->dialog), "command_entry"));

	g_return_val_if_fail (GTK_IS_CLIST (cdi->clist), FALSE);
	g_return_val_if_fail (GTK_IS_ENTRY (cdi->settings_entry), FALSE);
	g_return_val_if_fail (GTK_IS_ENTRY (cdi->command_entry), FALSE);
	
	/* Load Buttons */
	cdi->select_button = gpa_config_load_button (cdi, "select_button");
	cdi->copy_button   = gpa_config_load_button (cdi, "copy_button");
	cdi->delete_button = gpa_config_load_button (cdi, "delete_button");
	cdi->rename_button = gpa_config_load_button (cdi, "rename_button");
	
	if (!GTK_IS_BUTTON (cdi->select_button) ||
	    !GTK_IS_BUTTON (cdi->copy_button) ||
	    !GTK_IS_BUTTON (cdi->delete_button) ||
	    !GTK_IS_BUTTON (cdi->rename_button)) {
		gpa_error ("Button type error, expected gtk_button [%i]", __LINE__);
		return FALSE;
	}


	/* Connect the signals */
	gtk_signal_connect (GTK_OBJECT (cdi->clist), "select_row",
			    GTK_SIGNAL_FUNC (gpa_config_set_settings), cdi);
	gtk_signal_connect (GTK_OBJECT (cdi->clist), "unselect_row",
			    GTK_SIGNAL_FUNC (gpa_config_set_settings), cdi);
			    

	/* Entries */
	selected_settings = gpa_printer_settings_get_selected (cdi->printer);
	gtk_entry_set_text (GTK_ENTRY (cdi->settings_entry),
			    gpa_settings_get_name (selected_settings));
	gtk_entry_set_text (GTK_ENTRY (cdi->command_entry),
			    gpa_settings_get_command (selected_settings));

	/* Populate the clist */
	gpa_config_populate_settings_clist (cdi);
	
	gpa_config_command_entry_set (cdi);

	return TRUE;
}
