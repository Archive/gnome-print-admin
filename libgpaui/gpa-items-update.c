/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Copyright (C) 2000 Jose M Celorio
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Authors :
 *  Chema Celorio <chema@celorio.com>
 *
 */

#include "gpa-defs.h"

#include <glib.h>
#include <gtk/gtktogglebutton.h>
#include <gtk/gtkoptionmenu.h>
#include <gtk/gtkmenu.h>
#include <gtk/gtkcombo.h>
#include <gtk/gtkcheckbutton.h>
#include <gtk/gtkradiobutton.h>
#include <gtk/gtkspinbutton.h>

#include <libgnome/gnome-util.h>
#include <libgnomeui/gnome-dialog.h>
#include <libgnomeui/gnome-dialog-util.h>
#include <libgnomeui/gnome-uidefs.h>

#include "gpa-config.h"
#include "gpa-items-changed.h"
#include "gpa-items-update.h"

#include <libgpa/libgpa.h>

static void
gpa_gtk_radio_button_select (GSList *group, int n)
{
	GSList *l;
	int len = g_slist_length (group);

	if (n >= len || n < 0)
	{
		g_warning ("Cant' set the radio button. invalid number"
			   "n %i len %i\n", n, len);
		return;
	}
	
	l = g_slist_nth (group, len - n - 1);
	gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (l->data), 1);
}

static gint
gpa_config_settings_changed_element_option_menu (GpaConfigDialogInfo *cdi,
						 GtkOptionMenu *option_menu,
						 const gchar *path)
{
	GpaOption *option;
	GtkWidget *widget;
	GtkMenu *menu;
	GList *list;
	gint index = 0;
	gint selected = 0;
	
	g_return_val_if_fail (GTK_IS_OPTION_MENU (option_menu), -1);
	g_return_val_if_fail (GPA_IS_SETTINGS (cdi->loaded_settings), -1);
	g_return_val_if_fail (path != NULL, -1);

#if 0	
	if (strncmp (path, GPA_TAG_VALUE_PATH_PREFIX,
		     strlen (GPA_TAG_VALUE_PATH_PREFIX)) == 0) {
		gpa_error ("Invalid path for frame widget %s", path);
		return FALSE;
	}
#endif	
	
	menu = GTK_MENU (option_menu->menu);
	list = GTK_MENU_SHELL (menu)->children;

	g_return_val_if_fail (GTK_IS_MENU (menu), -1);
	g_return_val_if_fail (list != NULL, -1);
		
	for (; list != NULL; list = list->next) {
		widget = GTK_WIDGET (list->data);
		g_return_val_if_fail (GTK_IS_WIDGET (widget), -1);
		/* Change the settings it point to */
		option = gtk_object_get_data (GTK_OBJECT (widget), "option");
		g_return_val_if_fail (option != NULL, -1);
		if (gpa_option_is_selected (cdi->loaded_settings, option)) {
			selected = index;
		}
		index++;
	}

	gtk_option_menu_set_history (GTK_OPTION_MENU (option_menu), selected);
	
	return index;
}


static gint
gpa_config_settings_changed_element_combo (GpaConfigDialogInfo *cdi,
					   GtkCombo *combo)
{
	GpaOptions *options;
	GpaOption *option;

	g_return_val_if_fail (GTK_IS_COMBO (combo), -1);

	options = gtk_object_get_data (GTK_OBJECT (combo->entry), "options");

	g_return_val_if_fail (GPA_IS_OPTIONS (options), -1);

	option = gpa_options_get_selected_option (cdi->loaded_settings,
						  options,
						  FALSE);

	gtk_entry_set_text (GTK_ENTRY (combo->entry),
			    gpa_option_get_name (option));
	
	return 0;
}

static gint
gpa_config_settings_changed_element_check_button (GpaConfigDialogInfo *cdi,
						  GtkCheckButton *button)
{
	GpaOptions *options;
	GpaOption *option;
	gboolean active;
		
	g_return_val_if_fail (GTK_IS_CHECK_BUTTON (button), -1);

	options = gtk_object_get_data (GTK_OBJECT (button), "options");

	g_return_val_if_fail (GPA_IS_OPTIONS (options), -1);

	option = gpa_options_get_selected_option (cdi->loaded_settings,  options, FALSE);

	if (strcmp (gpa_option_get_id (option), GPA_TAG_TRUE)==0)
		active = TRUE;
	else
		active = FALSE;
		
	gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (button), active);
					   
	
	return 0;
}

static gboolean
gpa_config_settings_changed_element_radio_button (GpaConfigDialogInfo *cdi,
						  GtkRadioButton *radio_button)
{
	GpaOption *selected_option = NULL;
	GpaOption *option;
	GtkWidget *widget;
	GSList *list;
	gint index = 0;
	gint selected = 0;

	g_return_val_if_fail (GTK_IS_RADIO_BUTTON (radio_button), -1);
	g_return_val_if_fail (GPA_IS_SETTINGS (cdi->loaded_settings), -1);

	list = radio_button->group;
	for (; list != NULL; list = list->next) {
		widget = GTK_WIDGET (list->data);
		g_return_val_if_fail (GTK_IS_WIDGET (widget), -1);
		option = gtk_object_get_data (GTK_OBJECT (widget), "option");
		if (gpa_option_is_selected (cdi->loaded_settings, option)) {
			selected_option = option;
			selected = index;
		}
		index++;
	}

	g_return_val_if_fail (selected_option != NULL, FALSE);
	
	selected = g_slist_length (radio_button->group) - selected - 1;
	
	gpa_gtk_radio_button_select (radio_button->group, selected);

	/* Refresh the children frame */
	option = gtk_object_get_data (GTK_OBJECT (radio_button), "option");
	g_return_val_if_fail (option != NULL, FALSE);
	option = gpa_options_get_selected_option (cdi->loaded_settings,
						  gpa_option_get_parent (option),
						  FALSE);
	g_return_val_if_fail (option != NULL, FALSE);
	gpa_option_changed_update_children (cdi, GTK_WIDGET (radio_button), option);
	
	return index;
}
			

static gboolean
gpa_config_settings_changed_element_spin_button (GpaConfigDialogInfo *cdi,
						 GtkSpinButton *spin)
{
	GpaOption *option;
	GtkAdjustment *adjustment;
	gint value = 1;

	g_return_val_if_fail (GTK_IS_SPIN_BUTTON (spin), FALSE);
	g_return_val_if_fail (GPA_IS_SETTINGS (cdi->loaded_settings), FALSE);

	/* We need to load the spin button with the value .. */
	option = gtk_object_get_data (GTK_OBJECT (spin), "option");

	gpa_settings_value_get_from_option_int (cdi->loaded_settings,
						option, &value);

	adjustment = spin->adjustment;
	adjustment->value = value;

	gtk_adjustment_value_changed (adjustment);

	return TRUE;
}

static gboolean
gpa_config_settings_changed_element_entry (GpaConfigDialogInfo *cdi,
					   GtkEntry *entry)
{
	GpaOption *option;
	GtkAdjustment *adjustment;
	gdouble value = 1;

	g_return_val_if_fail (GTK_IS_ENTRY (entry), FALSE);
	g_return_val_if_fail (GPA_IS_SETTINGS (cdi->loaded_settings), FALSE);

	option = gtk_object_get_data (GTK_OBJECT (entry), "option");

	g_return_val_if_fail (GPA_IS_OPTION (option), FALSE);

	gpa_settings_value_get_from_option_double (cdi->loaded_settings, option, &value);

	adjustment = gtk_object_get_data (GTK_OBJECT (entry), "adjustment");

	g_return_val_if_fail (GTK_IS_ADJUSTMENT (adjustment), FALSE);
	
	adjustment->value = value;

	gtk_adjustment_value_changed (adjustment);

	return TRUE;
}

#if 0
static gboolean
gpa_config_settings_changed_element_frame (GpaConfigDialogInfo *cdi,
					   GtkFrame *frame,
					   const gchar *path)
{
	GtkWidget *table;

	g_return_val_if_fail (path != NULL, FALSE);
	g_return_val_if_fail (GTK_IS_FRAME (frame), FALSE);
	g_return_val_if_fail (GPA_IS_SETTINGS (cdi->loaded_settings), FALSE);

#if 0	
	if (strncmp (path, GPA_TAG_VALUE_PATH_PREFIX, strlen (GPA_TAG_VALUE_PATH_PREFIX)) != 0) {
		gpa_error ("Invalid path for frame widget %s", path);
		return FALSE;
	}

	if (strcmp (path + strlen (GPA_TAG_VALUE_PATH_PREFIX),
		    GPA_TAG_ORIENTATION) != 0) {
		gpa_error ("Dunno how to deal with this frame %s\n", path);
		return TRUE;
	}
#endif	

	table = GTK_BIN (frame)->child;

	g_return_val_if_fail (GTK_IS_TABLE (table), FALSE);

	gtk_widget_destroy (table);

	table = gpa_create_paper_orientation_table (cdi);

	g_return_val_if_fail (GTK_IS_TABLE (table), FALSE);
	
	gtk_container_add (GTK_CONTAINER (frame), table);

	gtk_widget_show_all (GTK_WIDGET (frame));

	return TRUE;
}
#endif

static void
gpa_config_settings_changed_element (gpointer key_in, gpointer value_in, gpointer data)
{
	GpaConfigDialogInfo * cdi = data;
	GtkWidget *widget;
	const gchar *path;

	path = (gchar *) key_in;
	widget = (GtkWidget *) value_in;

	g_return_if_fail (cdi != NULL);
	g_return_if_fail (GPA_IS_SETTINGS (cdi->loaded_settings));
	g_return_if_fail (GTK_IS_WIDGET (widget));
	
	if (!GTK_IS_WIDGET (widget)) {
		gpa_error ("Could not change settings, hash contains a "
			   "non-widget element");
		return;
	}

	if (GTK_IS_OPTION_MENU (widget)) {
		gpa_config_settings_changed_element_option_menu (
			cdi,
			GTK_OPTION_MENU (widget),
			path);
	} else if (GTK_IS_RADIO_BUTTON (widget)) {
		gpa_config_settings_changed_element_radio_button (
			cdi,
			GTK_RADIO_BUTTON (widget));
	} else if (GTK_IS_SPIN_BUTTON (widget)) {
		gpa_config_settings_changed_element_spin_button (
			cdi,
			GTK_SPIN_BUTTON (widget));
	} else if (GTK_IS_ENTRY (widget)) {
		gpa_config_settings_changed_element_entry (
			cdi,
			GTK_ENTRY (widget));
	} else if (GTK_IS_CHECK_BUTTON (widget)) {
		gpa_config_settings_changed_element_check_button (
			cdi,
			GTK_CHECK_BUTTON (widget));
#if 0	
	} else if (GTK_IS_FRAME (widget)) {
		gpa_config_settings_changed_element_frame (
			cdi,
			GTK_FRAME (widget),
			path);
#endif	
	} else if (GTK_IS_COMBO (widget)) {
		gpa_config_settings_changed_element_combo (
			cdi,
			GTK_COMBO (widget));
	} else 
		gpa_error ("Widget type unrecognized");

			
}


gboolean
gpa_config_settings_changed (GpaConfigDialogInfo *cdi, GpaSettings *settings)
{
	g_return_val_if_fail (GPA_IS_SETTINGS (settings), FALSE);

	cdi->loaded_settings = settings;
	
	g_hash_table_foreach (cdi->widgets, gpa_config_settings_changed_element, cdi);

	return TRUE;
}

