#ifndef __GPA_SETTINGS_CHANGED_H__
#define __GPA_SETTINGS_CHANGED_H__

BEGIN_GNOME_DECLS

extern gboolean   debug_turned_on;

#include <libgpa/gpa-settings.h>

gboolean gpa_config_settings_changed (GpaConfigDialogInfo *cdi, GpaSettings *settings);

END_GNOME_DECLS

#endif /* __GPA_SETTINGS_CHANGED_H___ */
