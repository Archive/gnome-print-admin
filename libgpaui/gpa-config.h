#ifndef __GNOME_PRINT_ADMIN_CONFIG_H__
#define __GNOME_PRINT_ADMIN_CONFIG_H__

BEGIN_GNOME_DECLS

extern gboolean   debug_turned_on;

#include <libgpa/gpa-printer.h>
#include <glade/glade-xml.h>

typedef struct _GpaConfigDialogInfo  GpaConfigDialogInfo;

struct  _GpaConfigDialogInfo {
	GpaPrinter *printer;
	GpaPrinter *real_printer;
	GpaSettings *selected_settings; /* The selected settings in the clist, NULL if there
					 * isn't a selected row. */
	GpaSettings *loaded_settings;   /* The settings loaded in the dialog */

	GHashTable *widgets;

	gboolean opened_by_app;
	
	gint page;

	GtkWidget *dialog;
	GtkWidget *notebook;
	
	GtkWidget *clist;

	GtkWidget *settings_entry;
	GtkWidget *command_entry;

	GtkWidget *ok_button;
	GtkWidget *cancel_button;
	
	GtkWidget *select_button;
	GtkWidget *copy_button;
	GtkWidget *delete_button;
	GtkWidget *rename_button;

	/* Rename Dailog */
	GtkWidget *rename_dialog;
	GtkWidget *rename_entry;
	GtkWidget *rename_label;
	
};


gboolean gpa_config_printer (GpaPrinter *printer, gboolean opened_by_app);
    void gpa_config_set_sensitivity (GpaConfigDialogInfo *cdi);

END_GNOME_DECLS

#endif /* __GNOME_PRINT_ADMIN_CONFIG_H__ */

