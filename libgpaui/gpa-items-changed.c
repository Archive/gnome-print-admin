/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Copyright (C) 2000 Jose M Celorio
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Authors :
 *  Chema Celorio <chema@celorio.com>
 *
 */

#include "gpa-defs.h"

#include <glib.h>
#include <gtk/gtkvbox.h>
#include <gtk/gtkhbox.h>
#include <gtk/gtktogglebutton.h>
#include <gtk/gtkcheckbutton.h>

#include <libgnome/gnome-util.h>
#include <libgnomeui/gnome-dialog.h>
#include <libgnomeui/gnome-dialog-util.h>
#include <libgnomeui/gnome-uidefs.h>

#include "gpa-config.h"
#include "gpa-slider.h"
#include "gpa-config-settings.h"

#include "gpa-items-create.h"
#include "gpa-items-changed.h"

#include <libgpa/libgpa.h>

static gboolean
gpa_option_emit_constraint_warning (GpaOption *option_changed,
				    GpaOption *option_in_conflict)
{
	GtkWidget *dialog;
	gchar *error;

	debug (FALSE, "");
	
	g_return_val_if_fail (option_changed     != NULL, FALSE);
	g_return_val_if_fail (option_in_conflict != NULL, FALSE);

	error =  g_strdup_printf ("\"%s\" conflicts with \"%s\"\n"
				  "(TODO :auto-solve conflicts)",
				  gpa_option_get_name (option_changed),
				  gpa_option_get_name (option_in_conflict));
	dialog = gnome_warning_dialog (error);
	gtk_widget_show (dialog);
	g_free (error);

	return TRUE;
}


/**
 * gpa_option_changed:
 * @widget: 
 * @cdi: 
 * 
 * Menu & radio buttons share this function
 **/
void 
gpa_option_changed (GtkWidget *widget, GpaConfigDialogInfo *cdi)
{
	GpaOption *conflicted_option;
	GpaOption *option;

	debug (FALSE, "");

	g_return_if_fail (cdi != NULL);
	g_return_if_fail (GPA_IS_SETTINGS (cdi->loaded_settings));

	g_print ("Gpa option changed !!!!!!!!!!!!!!\n");
	
	option   = gtk_object_get_data (GTK_OBJECT (widget), "option");

	g_return_if_fail (option != NULL);

	if (!gpa_settings_select_option (cdi->loaded_settings, option))
		return;
	
	gpa_option_changed_update_children (cdi, widget, option);

	conflicted_option = gpa_constraints_is_in_conflict (cdi->loaded_settings, option);

	if (conflicted_option != NULL)
		gpa_option_emit_constraint_warning (option, conflicted_option);
}


void
gpa_option_changed_numeric (GtkEditable *editable, GpaConfigDialogInfo *cdi)
{
	const GpaOptions *parent;
	GpaOption  *option;
	gchar *new_value;
	gchar *key;
	gint val;
	
	debug (FALSE, "");


	g_return_if_fail (cdi != NULL);
	g_return_if_fail (GPA_IS_SETTINGS (cdi->loaded_settings));
			  
	option = gtk_object_get_data (GTK_OBJECT (editable), "option");

	if (option == NULL) {
		gpa_error ("Could not get option from editable");
		return;
	}

	new_value = gtk_editable_get_chars (editable, 0, -1);
	val = atoi (new_value);
	g_free (new_value);
	new_value = g_strdup_printf ("%i", val);

	parent = gpa_option_get_parent (option);
	key = g_strdup_printf ("%s" GPA_PATH_DELIMITER "%s",
			       gpa_options_get_id (parent),
			       gpa_option_get_id (option));

	gpa_settings_value_replace (cdi->loaded_settings,
				    key,
				    new_value);

	/* FIXME: We are not checking for conflicts here  !!*/
	
	g_free (key);
	g_free (new_value);

}


void
gpa_orientation_toggled (GtkWidget *widget, GpaConfigDialogInfo *cdi)
{
	gchar *orientation_str;

	debug (FALSE, "");
	
	g_return_if_fail (GPA_IS_SETTINGS (cdi->loaded_settings));

	orientation_str = gpa_settings_value_dup (cdi->loaded_settings, GPA_TAG_ORIENTATION);
	if (orientation_str == NULL) {
		gpa_error ("Could not get the Orientation (6.0)\n");
		return;
	}
		
	if ( (strcmp (orientation_str, GPA_TAG_PORTRAIT) == 0))
		gpa_settings_value_replace (cdi->loaded_settings,
					    GPA_TAG_ORIENTATION,
					    GPA_TAG_LANDSCAPE);
	else if ( (strcmp (orientation_str, GPA_TAG_LANDSCAPE) == 0))
		gpa_settings_value_replace (cdi->loaded_settings,
					    GPA_TAG_ORIENTATION,
					    GPA_TAG_PORTRAIT);
	else if ( (strcmp (orientation_str, GPA_TAG_REVERSE_PORTRAIT) == 0))
		gpa_settings_value_replace (cdi->loaded_settings,
					    GPA_TAG_ORIENTATION,
					    GPA_TAG_REVERSE_LANDSCAPE);
	else if ( (strcmp (orientation_str, GPA_TAG_REVERSE_LANDSCAPE) == 0))
		gpa_settings_value_replace (cdi->loaded_settings,
					    GPA_TAG_ORIENTATION,
					    GPA_TAG_REVERSE_PORTRAIT);
	else {
		gpa_error ("Unrecognized orientation %s\n", orientation_str);
		return;
	}
}

void
gpa_rotate_toggled (GtkWidget *widget, GpaConfigDialogInfo *cdi)
{
	gchar *orientation_str;

	debug (FALSE, "");
	
	g_return_if_fail (GPA_IS_SETTINGS (cdi->loaded_settings));

	orientation_str = gpa_settings_value_dup (cdi->loaded_settings, GPA_TAG_ORIENTATION);
	if (orientation_str == NULL) {
		gpa_error ("Could not get the Orientation (6.1)\n");
		return;
	}

	if ( (strcmp (orientation_str, GPA_TAG_PORTRAIT) == 0))
		gpa_settings_value_replace (cdi->loaded_settings,
					    GPA_TAG_ORIENTATION,
					    GPA_TAG_REVERSE_PORTRAIT);
	else if ( (strcmp (orientation_str, GPA_TAG_LANDSCAPE) == 0))
		gpa_settings_value_replace (cdi->loaded_settings,
					    GPA_TAG_ORIENTATION,
					    GPA_TAG_REVERSE_LANDSCAPE);
	else if ( (strcmp (orientation_str, GPA_TAG_REVERSE_PORTRAIT) == 0))
		gpa_settings_value_replace (cdi->loaded_settings,
					    GPA_TAG_ORIENTATION,
					    GPA_TAG_PORTRAIT);
	else if ( (strcmp (orientation_str, GPA_TAG_REVERSE_LANDSCAPE) == 0))
		gpa_settings_value_replace (cdi->loaded_settings,
					    GPA_TAG_ORIENTATION,
					    GPA_TAG_LANDSCAPE);
	else {
		gpa_error ("Unrecognized orientation %s\n", orientation_str);
		return;
	}
}

/*
 *  === FRAME ==================================================
 *  =  ################### hbox ############################   =
 *  =  #  *** main_vbox ****   *** children_vbox*********  #   =
 *  =  #  *                *   *                        *  #   =
 *  =  #  * + r button     *   *  @@@ inner vbox @@@@   *  #   =
 *  =  #  * + r button     *   *  @                 @   *  #   =
 *  =  #  * + r button     *   *  @ + r button      @   *  #   =
 *  =  #  * + r button     *   *  @ + r button      @   *  #   = 
 *  =  #  * + r button     *   *  @ + r button      @   *  #   =
 *  =  #  * + r button     *   *  @                 @   *  #   = 
 *  =  #  *                *   *  @@@@@@@@@@@@@@@@@@@   *  #   =
 *  =  #  *                *   *                        *  #   =
 *  =  #  ******************   **************************  #   =
 *  =  #####################################################   =
 *  ============================================================
 *
 *
 */
void
gpa_option_changed_update_children (GpaConfigDialogInfo *cdi,
				    GtkWidget *button,
				    GpaOption *option)
{
	GtkWidget *main_vbox;
	GtkWidget *children_vbox;
	GtkWidget *inner_vbox;
	GList *children_list;

	debug (FALSE, "");

	g_return_if_fail (GTK_IS_WIDGET (button));
	g_return_if_fail (option != NULL);
	g_return_if_fail (GPA_IS_SETTINGS (cdi->loaded_settings));

	/* We need to find a pointer to the frame first widget */
	main_vbox = GTK_WIDGET (button->parent);
	children_vbox = gtk_object_get_data (GTK_OBJECT (main_vbox), "children_vbox");

	/* If "children_frame" was not found, then this options do not have
	   a children frame */
	if (children_vbox == NULL)
		return;

	children_list = GTK_BOX(children_vbox)->children;

	if (children_list != NULL) {
		inner_vbox = ((GtkBoxChild *)(children_list->data))->widget;
		if (inner_vbox != NULL) {
			g_return_if_fail (GTK_IS_WIDGET (inner_vbox));
			g_return_if_fail (GTK_IS_VBOX (inner_vbox));
			gtk_container_remove (GTK_CONTAINER (children_vbox), inner_vbox);
#if 0	
			/* Container remove, unrefs the widget. Verify with memprof (i.e. no leaking) */
			gtk_widget_destroy (inner_vbox);
#endif	
		}
	}

	/* If the new selected option is NULL, then we don't
	   need to create the new radio_buttons group */
	if (gpa_option_get_children (option) == NULL)
		return;
			
	inner_vbox = gpa_create_radio_buttons_from_options (cdi,
							    gpa_option_get_children (option),
							    FALSE);
	if (inner_vbox)
		gtk_box_pack_start (GTK_BOX (children_vbox), inner_vbox, FALSE, FALSE, 0);

	gtk_widget_show_all (inner_vbox);
}


void 
gpa_option_changed_combo_entry (GtkWidget *widget, GpaConfigDialogInfo *cdi)
{
	GpaOptions *options;
	GpaOption *conflicted_option;
	GpaOption *option = NULL;
	GList *list;
	const gchar * entry_text;

	debug (FALSE, "");

	g_return_if_fail (cdi != NULL);
	g_return_if_fail (GPA_IS_SETTINGS (cdi->loaded_settings));

	g_return_if_fail (GTK_IS_ENTRY (widget));
	   
	options   = gtk_object_get_data (GTK_OBJECT (widget), "options");

	g_return_if_fail (options != NULL);

	entry_text = gtk_entry_get_text (GTK_ENTRY (widget));

	/* Now get the option */
	list = gpa_options_get_children (options);
	for (; list != NULL; list = list->next) {
		option = (GpaOption *) list->data;
		if (strcmp (entry_text, gpa_option_get_name (option)) == 0)
			break;
	}

	if (!gpa_settings_select_option (cdi->loaded_settings, option))
		return;
	
	conflicted_option = gpa_constraints_is_in_conflict (cdi->loaded_settings, option);

	if (conflicted_option != NULL)
		gpa_option_emit_constraint_warning (option, conflicted_option);
}


void 
gpa_option_changed_boolean (GtkWidget *widget, GpaConfigDialogInfo *cdi)
{
	GpaOptions *options;
	GpaOption *conflicted_option;
	GpaOption *option;
	const gchar * true = GPA_TAG_TRUE;
	const gchar * false = GPA_TAG_FALSE;
	const gchar * id;

	debug (FALSE, "");

	g_return_if_fail (cdi != NULL);
	g_return_if_fail (GPA_IS_SETTINGS (cdi->loaded_settings));

	/* For now asume that a boolean option is a check_button
	 * I dunno if we will need to implement another widget type
	 * for boolean options*/

	g_return_if_fail (GTK_IS_CHECK_BUTTON (widget));
	   
	options   = gtk_object_get_data (GTK_OBJECT (widget), "options");

	g_return_if_fail (options != NULL);

	if (gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (widget)))
		id = true;
	else
		id = false;

	option = gpa_option_get_from_id (gpa_options_get_children (options), id);

	if (!gpa_settings_select_option (cdi->loaded_settings, option))
		return;
	
	conflicted_option = gpa_constraints_is_in_conflict (cdi->loaded_settings, option);

	if (conflicted_option != NULL)
		gpa_option_emit_constraint_warning (option, conflicted_option);
}
