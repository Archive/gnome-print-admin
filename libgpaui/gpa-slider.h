#ifndef __GNOME_PRINT_ADMIN_SLIDER_H__
#define __GNOME_PRINT_ADMIN_SLIDER_H__

BEGIN_GNOME_DECLS

extern gboolean   debug_turned_on;

#include <gtk/gtkwidget.h>
#include <gtk/gtkadjustment.h>
#include <gtk/gtkentry.h>

#include <libgpa/gpa-option.h>
#include <libgpa/gpa-settings.h>
#include "gpa-config.h"

GtkWidget * gpa_slider (GpaConfigDialogInfo *cdi,
				    GpaOption *option,
				    GtkAdjustment *adjustment,
				    GtkEntry **entry_);

END_GNOME_DECLS

#endif /* __GNOME_PRINT_ADMIN_SLIDER_H__ */



