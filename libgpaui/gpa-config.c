/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*- */
/*
 * Copyright (C) 2000 Jose M Celorio
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Authors :
 *  Chema Celorio <chema@celorio.com>
 *
 */

#include "gpa-defs.h"

#include <glib.h>
#include <gtk/gtkmain.h>
#include <gtk/gtkwidget.h>
#include <gtk/gtkclist.h>
#include <gtk/gtknotebook.h>
#include <gtk/gtkbox.h>
#include <gtk/gtkhbox.h>
#include <gtk/gtkvbox.h>
#include <gtk/gtkbbox.h>
#include <gtk/gtktable.h>
#include <gtk/gtkentry.h>
#include <gtk/gtkscrolledwindow.h>

#include <libgnome/gnome-util.h>
#include <libgnomeui/gnome-dialog.h>
#include <libgnomeui/gnome-dialog-util.h>
#include <libgnomeui/gnome-uidefs.h>
#include <libgnomeui/gnome-stock.h>
 
#include "gpa-config.h"
#include "gpa-config-settings.h"
#include "gpa-items-create.h"
#include "gpa-items-update.h"

#include <libgpa/libgpa.h>

static void
gpa_dialog_destroy (GtkWidget *dialog, gpointer data)
{
	debug (FALSE, "");
	g_return_if_fail (GNOME_IS_DIALOG (dialog));
	gtk_widget_destroy (dialog);
}

static void
gpa_config_dialog_destroy (GtkWidget *dialog, gpointer data)
{
	debug (FALSE, "");
	g_return_if_fail (GNOME_IS_DIALOG (dialog));
	gtk_widget_destroy (dialog);
	gtk_main_quit ();
}

void
gpa_config_set_sensitivity (GpaConfigDialogInfo *cdi)
{
	gboolean selected;
	
	g_return_if_fail (cdi != NULL);
	g_return_if_fail (GNOME_IS_DIALOG (cdi->dialog));

	selected = (cdi->selected_settings == NULL) ? FALSE : TRUE;

	g_return_if_fail (GTK_IS_BUTTON (cdi->select_button));
	
	gtk_widget_set_sensitive (cdi->select_button, selected);
	gtk_widget_set_sensitive (cdi->copy_button, selected);
	gtk_widget_set_sensitive (cdi->rename_button, selected);

	/* We need 2 settings to enable the delete button */
	if (g_list_length (GTK_CLIST (cdi->clist)->row_list) < 2)
		selected = FALSE;
	gtk_widget_set_sensitive (cdi->delete_button, selected);

}

static gboolean
gpa_load_general_page (GpaConfigDialogInfo *cdi)
{
	const gchar *printer_name;
	const gchar *model_name;
	const GpaModel *model;
	GpaPrinter *printer;
	GtkWidget *printer_label;
	GtkWidget *model_label;

	debug (FALSE, "");

	g_return_val_if_fail (cdi != NULL, FALSE);
	g_return_val_if_fail (GPA_IS_PRINTER (cdi->printer), FALSE);

	printer_label    = GTK_WIDGET (gtk_object_get_data (GTK_OBJECT (cdi->dialog), "printer_label"));
	model_label      = GTK_WIDGET (gtk_object_get_data (GTK_OBJECT (cdi->dialog), "model_label"));

	/* This type check is like cheking for NULL, only better */
	g_return_val_if_fail (GTK_IS_LABEL (printer_label), FALSE);
	g_return_val_if_fail (GTK_IS_LABEL (model_label), FALSE);

	/* Set the labels */
	printer = cdi->printer;
	printer_name = gpa_printer_get_name (printer);
	model        = gpa_printer_get_model (printer);
	model_name   = gpa_model_get_info (model, GPA_TAG_MODEL_NAME);

	if ( (printer_name == NULL) ||
	     (model_name   == NULL))
		gpa_error ("Could not get the required strings from the printer "
			   "to create the general page");


	gtk_label_set_text (GTK_LABEL (printer_label), printer_name);
	gtk_label_set_text (GTK_LABEL (model_label),   model_name);

	gpa_load_general_page_settings_stuff (cdi);
#if 0	
	gpa_config_backend_option_menu_attach (cdi->printer, table, cdi->widgets);
#endif

	return TRUE;
}

static void
gpa_config_ok_clicked (gpointer dummy, GpaConfigDialogInfo *cdi)
{
	g_return_if_fail (cdi != NULL);
	g_return_if_fail (GPA_IS_PRINTER (cdi->printer));


	if (!gpa_settings_list_verify ( gpa_printer_settings_list_get (cdi->printer), FALSE)) {
		GtkWidget *err_dialog;
		gchar *error;
		error =  g_strdup_printf ("Conflicting options where found in "
					  "the printer configuration.\n"
					  "(TODO : this should not happen "
					  "since the conflicts are going to be "
					  "auto-solved	)");
		err_dialog = gnome_warning_dialog (error);
		gtk_widget_show (err_dialog);
		g_free (error);
		return;
	}
	gpa_printer_settings_list_swap (cdi->real_printer, cdi->printer);
	/* FIXME :
	   gtk_object_unref (printer);
	*/
	gpa_dialog_destroy (cdi->dialog, NULL);
}

static void
gpa_config_cancel_clicked (gpointer dummy, GpaConfigDialogInfo *cdi)
{
	g_return_if_fail (cdi != NULL);
	g_return_if_fail (GPA_IS_PRINTER (cdi->printer));

	gpa_dialog_destroy (cdi->dialog, NULL);
}

/**
 * create_config_dialog:
 * @void: 
 * 
 * As you may be able to tell, this dialog was glade generated. I am not sure
 * if it is worth it to clean it up or return to libglade
 * 
 * Return Value: 
 **/
static GtkWidget*
create_config_dialog (void)
{
	GtkWidget *config_dialog;
	GtkWidget *dialog_vbox3;
	GtkWidget *hbox2;
	GtkWidget *notebook;
	GtkWidget *vbox7;
	GtkWidget *table4;
	GtkWidget *model_label;
	GtkWidget *printer_label;
	GtkWidget *table3;
	GtkWidget *label30;
	GtkWidget *label31;
	GtkWidget *select_button;
	GtkWidget *copy_button;
	GtkWidget *delete_button;
	GtkWidget *rename_button;
	GtkWidget *settings_entry;
	GtkWidget *scrolledwindow5;
	GtkWidget *settings_clist;
	GtkWidget *label18;
	GtkWidget *hbox4;
	GtkWidget *command_entry;
	GtkWidget *label10;
	GtkWidget *dialog_action_area3;
	GtkWidget *button14;
	GtkWidget *button16;

	config_dialog = gnome_dialog_new (NULL, NULL);
	gtk_object_set_data (GTK_OBJECT (config_dialog), "config_dialog", config_dialog);
	gtk_widget_set_usize (config_dialog, 430, -2);
	gtk_window_set_position (GTK_WINDOW (config_dialog), GTK_WIN_POS_MOUSE);
	gtk_window_set_default_size (GTK_WINDOW (config_dialog), 397, 438);
	
	dialog_vbox3 = GNOME_DIALOG (config_dialog)->vbox;
	gtk_object_set_data (GTK_OBJECT (config_dialog), "dialog_vbox3", dialog_vbox3);
	gtk_widget_show (dialog_vbox3);
	
	hbox2 = gtk_hbox_new (FALSE, 0);
	gtk_widget_ref (hbox2);
	gtk_object_set_data_full (GTK_OBJECT (config_dialog), "hbox2", hbox2,
				  (GtkDestroyNotify) gtk_widget_unref);
	gtk_widget_show (hbox2);
	gtk_box_pack_start (GTK_BOX (dialog_vbox3), hbox2, TRUE, TRUE, 0);
	
	notebook = gtk_notebook_new ();
	gtk_widget_ref (notebook);
	gtk_object_set_data_full (GTK_OBJECT (config_dialog), "notebook", notebook,
				  (GtkDestroyNotify) gtk_widget_unref);
	gtk_widget_show (notebook);
	gtk_box_pack_start (GTK_BOX (hbox2), notebook, TRUE, TRUE, 0);
	
	vbox7 = gtk_vbox_new (FALSE, 20);
	gtk_widget_ref (vbox7);
	gtk_object_set_data_full (GTK_OBJECT (config_dialog), "vbox7", vbox7,
				  (GtkDestroyNotify) gtk_widget_unref);
	gtk_container_add (GTK_CONTAINER (notebook), vbox7);
	
	table4 = gtk_table_new (2, 1, FALSE);
	gtk_widget_ref (table4);
	gtk_object_set_data_full (GTK_OBJECT (config_dialog), "table4", table4,
				  (GtkDestroyNotify) gtk_widget_unref);
	gtk_widget_show (table4);
	gtk_box_pack_start (GTK_BOX (vbox7), table4, FALSE, FALSE, 0);
	gtk_container_set_border_width (GTK_CONTAINER (table4), 25);
	gtk_table_set_row_spacings (GTK_TABLE (table4), 15);
	gtk_table_set_col_spacings (GTK_TABLE (table4), 50);
	
	model_label = gtk_label_new ("(Model Name)");
	gtk_widget_ref (model_label);
	gtk_object_set_data_full (GTK_OBJECT (config_dialog), "model_label", model_label,
				  (GtkDestroyNotify) gtk_widget_unref);
	gtk_widget_show (model_label);
	gtk_table_attach (GTK_TABLE (table4), model_label, 0, 1, 1, 2,
			  (GtkAttachOptions) (GTK_FILL),
			  (GtkAttachOptions) (0), 0, 0);
	
	printer_label = gtk_label_new ("My Printer");
	gtk_widget_ref (printer_label);
	gtk_object_set_data_full (GTK_OBJECT (config_dialog), "printer_label", printer_label,
				  (GtkDestroyNotify) gtk_widget_unref);
	gtk_widget_show (printer_label);
	gtk_table_attach (GTK_TABLE (table4), printer_label, 0, 1, 0, 1,
			  (GtkAttachOptions) (GTK_EXPAND | GTK_FILL),
			  (GtkAttachOptions) (0), 0, 0);
	
	table3 = gtk_table_new (3, 2, FALSE);
	gtk_widget_ref (table3);
	gtk_object_set_data_full (GTK_OBJECT (config_dialog), "table3", table3,
				  (GtkDestroyNotify) gtk_widget_unref);
	gtk_widget_show (table3);
	gtk_box_pack_start (GTK_BOX (vbox7), table3, TRUE, TRUE, 0);
	gtk_table_set_row_spacings (GTK_TABLE (table3), 10);
	gtk_table_set_col_spacings (GTK_TABLE (table3), 6);
	
	label30 = gtk_label_new ("Command :");
	gtk_widget_ref (label30);
	gtk_object_set_data_full (GTK_OBJECT (config_dialog), "label30", label30,
				  (GtkDestroyNotify) gtk_widget_unref);
	gtk_widget_show (label30);
	gtk_table_attach (GTK_TABLE (table3), label30, 0, 1, 0, 1,
			  (GtkAttachOptions) (GTK_FILL),
			  (GtkAttachOptions) (0), 0, 0);
	gtk_misc_set_alignment (GTK_MISC (label30), 1, 0.5);
	
	label31 = gtk_label_new ("Loaded Settings : ");
	gtk_widget_ref (label31);
	gtk_object_set_data_full (GTK_OBJECT (config_dialog), "label31", label31,
				  (GtkDestroyNotify) gtk_widget_unref);
	gtk_widget_show (label31);
	gtk_table_attach (GTK_TABLE (table3), label31, 0, 1, 1, 2,
			  (GtkAttachOptions) (GTK_FILL),
			  (GtkAttachOptions) (0), 0, 0);
	gtk_misc_set_alignment (GTK_MISC (label31), 1, 0.5);
	
	vbox7 = gtk_vbox_new (FALSE, 10);
	gtk_widget_ref (vbox7);
	gtk_object_set_data_full (GTK_OBJECT (config_dialog), "vbox7", vbox7,
				  (GtkDestroyNotify) gtk_widget_unref);
	gtk_widget_show (vbox7);
	gtk_table_attach (GTK_TABLE (table3), vbox7, 0, 1, 2, 3,
			  (GtkAttachOptions) (0),
			  (GtkAttachOptions) (0), 0, 0);
	
	select_button = gtk_button_new_with_label ("Load");
	gtk_widget_ref (select_button);
	gtk_object_set_data_full (GTK_OBJECT (config_dialog), "select_button", select_button,
				  (GtkDestroyNotify) gtk_widget_unref);
	gtk_widget_show (select_button);
	gtk_box_pack_start (GTK_BOX (vbox7), select_button, FALSE, FALSE, 0);
	
	copy_button = gtk_button_new_with_label ("Copy");
	gtk_widget_ref (copy_button);
	gtk_object_set_data_full (GTK_OBJECT (config_dialog), "copy_button", copy_button,
				  (GtkDestroyNotify) gtk_widget_unref);
	gtk_widget_show (copy_button);
	gtk_box_pack_start (GTK_BOX (vbox7), copy_button, FALSE, FALSE, 0);
	
	delete_button = gtk_button_new_with_label ("Delete ");
	gtk_widget_ref (delete_button);
	gtk_object_set_data_full (GTK_OBJECT (config_dialog), "delete_button", delete_button,
				  (GtkDestroyNotify) gtk_widget_unref);
	gtk_widget_show (delete_button);
	gtk_box_pack_start (GTK_BOX (vbox7), delete_button, FALSE, FALSE, 0);
	
	rename_button = gtk_button_new_with_label ("Rename");
	gtk_widget_ref (rename_button);
	gtk_object_set_data_full (GTK_OBJECT (config_dialog), "rename_button", rename_button,
				  (GtkDestroyNotify) gtk_widget_unref);
	gtk_widget_show (rename_button);
	gtk_box_pack_start (GTK_BOX (vbox7), rename_button, FALSE, FALSE, 0);
	
	settings_entry = gtk_entry_new ();
	gtk_widget_ref (settings_entry);
	gtk_object_set_data_full (GTK_OBJECT (config_dialog), "settings_entry", settings_entry,
				  (GtkDestroyNotify) gtk_widget_unref);
	gtk_widget_show (settings_entry);
	gtk_table_attach (GTK_TABLE (table3), settings_entry, 1, 2, 1, 2,
			  (GtkAttachOptions) (0),
			  (GtkAttachOptions) (0), 0, 0);
	gtk_widget_set_usize (settings_entry, 219, -2);
	gtk_entry_set_editable (GTK_ENTRY (settings_entry), FALSE);
	
	scrolledwindow5 = gtk_scrolled_window_new (NULL, NULL);
	gtk_widget_ref (scrolledwindow5);
	gtk_object_set_data_full (GTK_OBJECT (config_dialog), "scrolledwindow5", scrolledwindow5,
				  (GtkDestroyNotify) gtk_widget_unref);
	gtk_widget_show (scrolledwindow5);
	gtk_table_attach (GTK_TABLE (table3), scrolledwindow5, 1, 2, 2, 3,
			  (GtkAttachOptions) (0),
			  (GtkAttachOptions) (GTK_FILL), 0, 0);
	gtk_widget_set_usize (scrolledwindow5, 219, -2);
	gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (scrolledwindow5), GTK_POLICY_NEVER, GTK_POLICY_AUTOMATIC);
	
	settings_clist = gtk_clist_new (1);
	gtk_widget_ref (settings_clist);
	gtk_object_set_data_full (GTK_OBJECT (config_dialog), "settings_clist", settings_clist,
				  (GtkDestroyNotify) gtk_widget_unref);
	gtk_widget_show (settings_clist);
	gtk_container_add (GTK_CONTAINER (scrolledwindow5), settings_clist);
	gtk_widget_set_usize (settings_clist, 219, -2);
	gtk_clist_set_column_width (GTK_CLIST (settings_clist), 0, 80);
	gtk_clist_column_titles_hide (GTK_CLIST (settings_clist));
	
	label18 = gtk_label_new ("label18");
	gtk_widget_ref (label18);
	gtk_object_set_data_full (GTK_OBJECT (config_dialog), "label18", label18,
				  (GtkDestroyNotify) gtk_widget_unref);
	gtk_widget_show (label18);
	gtk_clist_set_column_widget (GTK_CLIST (settings_clist), 0, label18);
	
	hbox4 = gtk_hbox_new (FALSE, 0);
	gtk_widget_ref (hbox4);
	gtk_object_set_data_full (GTK_OBJECT (config_dialog), "hbox4", hbox4,
				  (GtkDestroyNotify) gtk_widget_unref);
	gtk_widget_show (hbox4);
	gtk_table_attach (GTK_TABLE (table3), hbox4, 1, 2, 0, 1,
			  (GtkAttachOptions) (GTK_FILL),
			  (GtkAttachOptions) (GTK_FILL), 0, 0);
	
	command_entry = gtk_entry_new ();
	gtk_widget_ref (command_entry);
	gtk_object_set_data_full (GTK_OBJECT (config_dialog), "command_entry", command_entry,
				  (GtkDestroyNotify) gtk_widget_unref);
	gtk_widget_show (command_entry);
	gtk_box_pack_start (GTK_BOX (hbox4), command_entry, FALSE, TRUE, 0);
	gtk_widget_set_usize (command_entry, 100, -2);
	
	label10 = gtk_label_new ("General");
	gtk_widget_ref (label10);
	gtk_object_set_data_full (GTK_OBJECT (config_dialog), "label10", label10,
				  (GtkDestroyNotify) gtk_widget_unref);
	gtk_widget_show (label10);
	gtk_notebook_set_tab_label (GTK_NOTEBOOK (notebook), gtk_notebook_get_nth_page (GTK_NOTEBOOK (notebook), 0), label10);
	
	dialog_action_area3 = GNOME_DIALOG (config_dialog)->action_area;
	gtk_object_set_data (GTK_OBJECT (config_dialog), "dialog_action_area3", dialog_action_area3);
	gtk_widget_show (dialog_action_area3);
	gtk_button_box_set_layout (GTK_BUTTON_BOX (dialog_action_area3), GTK_BUTTONBOX_END);
	gtk_button_box_set_spacing (GTK_BUTTON_BOX (dialog_action_area3), 8);
	
	gnome_dialog_append_button (GNOME_DIALOG (config_dialog), GNOME_STOCK_BUTTON_OK);
	button14 = GTK_WIDGET (g_list_last (GNOME_DIALOG (config_dialog)->buttons)->data);
	gtk_widget_ref (button14);
	gtk_object_set_data_full (GTK_OBJECT (config_dialog), "ok_button", button14,
				  (GtkDestroyNotify) gtk_widget_unref);
	gtk_widget_show (button14);
	GTK_WIDGET_SET_FLAGS (button14, GTK_CAN_DEFAULT);
	
	gnome_dialog_append_button (GNOME_DIALOG (config_dialog), GNOME_STOCK_BUTTON_CANCEL);
	button16 = GTK_WIDGET (g_list_last (GNOME_DIALOG (config_dialog)->buttons)->data);
	gtk_widget_ref (button16);
	gtk_object_set_data_full (GTK_OBJECT (config_dialog), "cancel_button", button16,
				  (GtkDestroyNotify) gtk_widget_unref);
	gtk_widget_show (button16);
	GTK_WIDGET_SET_FLAGS (button16, GTK_CAN_DEFAULT);
	
	return config_dialog;
}

static GtkWidget *
gpa_config_dialog_load_pages (GpaConfigDialogInfo *cdi)
{
	GpaSettings *settings;
	gchar *title;

	debug (FALSE, "");

	g_return_val_if_fail (GPA_IS_PRINTER (cdi->printer), FALSE);
	g_return_val_if_fail (GPA_IS_PRINTER (cdi->real_printer), FALSE);
	
	cdi->dialog   = create_config_dialog ();
	cdi->notebook = GTK_WIDGET (gtk_object_get_data (GTK_OBJECT (cdi->dialog), "notebook"));
	
	/* This is like checking for null, but we also check type */
	if (!GNOME_IS_DIALOG (cdi->dialog)) {
		gpa_error ("config_dialog type error, expected gnome_dialog");
		return FALSE;
	}
	if (!GTK_IS_NOTEBOOK (cdi->notebook)) {
		gpa_error ("notebook type error, expected gtk_notebook");
		return FALSE;
	}

	/*connect signals */
	gtk_signal_connect (GTK_OBJECT (cdi->dialog), "destroy",
			    GTK_SIGNAL_FUNC (gpa_config_dialog_destroy),  NULL);
	gtk_signal_connect (GTK_OBJECT (cdi->dialog), "delete_event",
			    GTK_SIGNAL_FUNC (gtk_false),  NULL);

	/* Set the window title */
	title = g_strdup_printf ("Printer Configuration : %s", gpa_printer_get_name (cdi->printer));
	gtk_window_set_title (GTK_WINDOW (cdi->dialog), title);
	g_free (title);

	settings = gpa_printer_settings_get_selected (cdi->printer);
	
	cdi->widgets = g_hash_table_new (g_str_hash, g_str_equal);
	cdi->page = 1;
	cdi->loaded_settings = settings;
	cdi->selected_settings = NULL;

	/* Now load each page */
	if (!gpa_load_general_page (cdi))
		return FALSE;
	if (!gpa_load_paper_page   (cdi))
		return FALSE;
	if (!gpa_load_generic_page (cdi, GPA_GROUP_QUALITY, _("Print Quality")))
		return FALSE;
	if (!gpa_load_generic_page (cdi, GPA_GROUP_COMPRESSION, _("Compression")))
		return FALSE;
	if (!gpa_load_generic_page (cdi, GPA_GROUP_PS, _("Postscript")))
		return FALSE;
	if (!gpa_load_generic_page (cdi, GPA_GROUP_INSTALLED, _("Installed Options")))
		return FALSE;

	/* Load Buttons, the connect signal is not working, I guess it has to
	 * do with the fact that it is a gnome-dialog
	 */
	cdi->ok_button     = GTK_WIDGET (gtk_object_get_data (GTK_OBJECT (cdi->dialog), "ok_button"));
	cdi->cancel_button = GTK_WIDGET (gtk_object_get_data (GTK_OBJECT (cdi->dialog), "cancel_button"));
	/* Connect by hand */
	gnome_dialog_button_connect (GNOME_DIALOG (cdi->dialog), 0,
				     gpa_config_ok_clicked, cdi);
	gnome_dialog_button_connect (GNOME_DIALOG (cdi->dialog), 1,
				     gpa_config_cancel_clicked, cdi);

	/* Emit a changed settings signal to verify that everything
	 * is fine (widgets loaded and with valid pointers). Set to
	 * NULL afterwards since ther isn't a selected settings in the clist
	 */
	gpa_config_settings_changed (cdi, cdi->loaded_settings);
	cdi->selected_settings = NULL;
	
        /* Set sensitivity */
	gpa_config_set_sensitivity (cdi);
		
	return cdi->dialog;
}

static GtkWidget *
gpa_config_dialog_create (GpaPrinter *printer, GpaPrinter *real_printer, gboolean opened_by_app)
{
	GpaConfigDialogInfo *cdi;
	GtkWidget *dialog;
	
	debug (FALSE, "");

	g_return_val_if_fail (GPA_IS_PRINTER (printer), FALSE);

	cdi = g_new (GpaConfigDialogInfo, 1);
	cdi->printer = printer;
	cdi->real_printer = real_printer;
	cdi->rename_dialog = NULL;
	cdi->selected_settings = NULL;
	cdi->loaded_settings = NULL;

	cdi->settings_entry = NULL;
	cdi->command_entry = NULL;
	
	cdi->ok_button = NULL;
	cdi->cancel_button = NULL;
	cdi->select_button = NULL;
	cdi->delete_button = NULL;
	cdi->rename_button = NULL;
	cdi->opened_by_app = opened_by_app;
		
	dialog = gpa_config_dialog_load_pages (cdi);

	return dialog;
}

gboolean
gpa_config_printer (GpaPrinter *printer, gboolean opened_by_app)
{
	GpaPrinter *copy_of_printer;
	GtkWidget *dialog;

	g_return_val_if_fail (GPA_IS_PRINTER (printer), FALSE);
	
	/* This is what we do :
	   1. We make a copy of the settings, which we use to build the dialog
	   2. If the user cancels the properties dialog. We just
	   dispose, the copy.
	   3. If the user clicks [OK], we swap the settings, and dispose the copy
	*/
	copy_of_printer = gpa_printer_copy (printer);

	g_return_val_if_fail (GPA_IS_PRINTER (copy_of_printer), FALSE);
			      
	dialog = gpa_config_dialog_create (copy_of_printer, printer, opened_by_app);

	gtk_window_set_modal (GTK_WINDOW (dialog), TRUE);
	gtk_widget_show_all (dialog);

	gtk_main ();

	return TRUE;
}

/* IMPORTANT : This struct is a mirror of the one in libgnomeprint/gnome-print-dialog.c
 * keep them in sync */
typedef struct _GpaConfigDlopenData GpaConfigDlopenData;
struct _GpaConfigDlopenData
{
	gint (*gpa_config_printer) (GpaPrinter *printer, gboolean opened_by_app);
	gint (*init_gpa_config_printer) (GpaConfigDlopenData *pd);
};

gint init_gpa_config_printer (GpaConfigDlopenData *pd);

gint
init_gpa_config_printer (GpaConfigDlopenData *pd)
{
	pd->gpa_config_printer = gpa_config_printer;
	return TRUE;
}
