#ifndef __GNOME_PRINT_ADMIN_CONFIG_CREATE_H__
#define __GNOME_PRINT_ADMIN_CONFIG_CREATE_H__

BEGIN_GNOME_DECLS

extern gboolean   debug_turned_on;

#include "gpa-config.h"
#include <libgpa/gpa-options.h>

gboolean gpa_load_generic_page (GpaConfigDialogInfo *cdi,
						  GpaOptionsGroup group,
						  const gchar *page_name);

gboolean gpa_load_paper_page (GpaConfigDialogInfo *cdi);

GtkWidget* gpa_create_radio_buttons_from_options (GpaConfigDialogInfo *cdi,
										const GpaOptions *options,
										gboolean homogeneous);


END_GNOME_DECLS

#endif /* __GNOME_PRINT_ADMIN_CONFIG_CREATE_H__ */

